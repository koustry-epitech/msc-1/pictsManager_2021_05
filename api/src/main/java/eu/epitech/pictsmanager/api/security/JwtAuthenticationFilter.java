package eu.epitech.pictsmanager.api.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 15 /06/2020
 */
@Component
public class JwtAuthenticationFilter extends GenericFilterBean {

  /** The constant BEARER. */
  public static final String BEARER = "Bearer";

  private Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
  @Autowired private AuthenticationProvider authenticationProvider;

  @Autowired private JwtTokenProvider jwtTokenProvider;

  /** Instantiates a new Jwt authentication token filter. */
  public JwtAuthenticationFilter() {}

  @Override
  public void doFilter(
      ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
      throws IOException, ServletException {
    final HttpServletRequest request = (HttpServletRequest) servletRequest;
    final HttpServletResponse response = (HttpServletResponse) servletResponse;
    final Optional<String> token =
        Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION));

    if (token.isPresent() && token.get().startsWith(BEARER)) {
      logger.info("Retrieving BEARER token {}", token.get());
      String bearerToken = token.get().substring(BEARER.length() + 1);
      try {
        Jws<Claims> claimsJws = jwtTokenProvider.validateJwtToken(bearerToken);
        Authentication authentication = authenticationProvider.getAuthentication(claimsJws);
        SecurityContextHolder.getContext().setAuthentication(authentication);
      } catch (ExpiredJwtException exception) {
        logger.error("ExpiredJwtException {}", exception.getMessage());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response
            .getWriter()
            .write(String.format("ExpiredJwtException: %s", exception.getMessage()));
        return;
      } catch (JwtException exception) {
        logger.error("JwtException {}", exception.getMessage());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().write(String.format("JwtException: %s", exception.getMessage()));
        return;
      }
    }
    chain.doFilter(servletRequest, servletResponse);
    SecurityContextHolder.getContext().setAuthentication(null);
  }
}
