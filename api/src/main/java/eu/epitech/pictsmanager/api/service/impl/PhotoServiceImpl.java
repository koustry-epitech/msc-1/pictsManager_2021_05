package eu.epitech.pictsmanager.api.service.impl;

import com.mongodb.client.gridfs.model.GridFSFile;
import eu.epitech.pictsmanager.api.exception.IllegalResourceAccessException;
import eu.epitech.pictsmanager.api.exception.ResourceNotFoundException;
import eu.epitech.pictsmanager.api.model.Metadata;
import eu.epitech.pictsmanager.api.model.Photo;
import eu.epitech.pictsmanager.api.model.type.AccessStatusType;
import eu.epitech.pictsmanager.api.model.type.ResourceType;
import eu.epitech.pictsmanager.api.repository.MetadataRepository;
import eu.epitech.pictsmanager.api.repository.PhotoRepository;
import eu.epitech.pictsmanager.api.service.AccessService;
import eu.epitech.pictsmanager.api.service.PhotoService;
import eu.epitech.pictsmanager.api.service.UserService;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
@Service
public class PhotoServiceImpl implements PhotoService {

  private final PhotoRepository photoRepository;
  private final MetadataRepository metadataRepository;
  private final AccessService accessService;

  @Autowired private GridFsTemplate gridFsTemplate;

  @Autowired private UserService userService;

  @Autowired
  public PhotoServiceImpl(
      PhotoRepository photoRepository,
      MetadataRepository metadataRepository,
      AccessService accessService) {
    this.photoRepository = photoRepository;
    this.metadataRepository = metadataRepository;
    this.accessService = accessService;
  }

  @Override
  public List<Photo> getAllPhotos() {
    return photoRepository.findAll();
  }

  @Override
  @Deprecated
  public List<String> getAllPhotosIds() {
    return photoRepository.findAllPhotosIds();
  }

  @Override
  public List<String> getAllPhotosIds(String userId) {
    List<String> photosIds = photoRepository.findAllPhotosIds();
    return photosIds.stream()
        .filter(photoId -> accessService.canRead(userId, photoId))
        .collect(Collectors.toList());
  }

  @Override
  public Optional<Photo> getPhotoById(String photoId, String userId)
      throws IllegalResourceAccessException {
    if (!accessService.canRead(userId, photoId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] cannot access to photo [id: %s]", userId, photoId));
    return photoRepository.findById(photoId);
  }

  @Override
  public InputStream getPhotoData(Photo photo) {
    String gridfsPictureId = photo.getGridfsPictureId();
    if (gridfsPictureId == null) {
      return new ByteArrayInputStream(photo.getData().getData());
    } else {
      try {
        GridFSFile gridFsFile =
            gridFsTemplate.findOne(new Query(Criteria.where("_id").is(gridfsPictureId)));
        return gridFsTemplate.getResource(gridFsFile).getInputStream();
      } catch (IOException ioex) {
        throw new RuntimeException("Error while loding the picture", ioex);
      }
    }
  }

  @Override
  @Deprecated
  public Metadata getPhotoMetadata(String photoId) {
    Photo photo =
        photoRepository
            .findById(photoId)
            .orElseThrow(() -> new RuntimeException("No image uploaded!"));
    return photo.getMetadata();
  }

  @Override
  public Metadata getPhotoMetadataByUserAccess(String photoId, String userId)
      throws IllegalResourceAccessException {
    if (!accessService.canRead(userId, photoId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] cannot access to photo metadata [id: %s]", userId, photoId));
    Photo photo =
        photoRepository
            .findById(photoId)
            .orElseThrow(() -> new RuntimeException("No image uploaded!"));
    return photo.getMetadata();
  }

  @Override
  public Photo addPhoto(Long contentLength, InputStream picture, String userId) throws IOException {
    Photo photo = new Photo();

    photo.setSize(contentLength);
    if (contentLength >= 1024 * 1024 * 16) { // >= 16 MB
      ObjectId gridfsPicture = gridFsTemplate.store(picture, UUID.randomUUID().toString() + ".jpg");
      photo.setGridfsPictureId(gridfsPicture.toString());
    } else {
      Binary data = new Binary(BsonBinarySubType.BINARY, picture.readAllBytes());
      photo.setGridfsPictureId(null);
      photo.setData(data);
    }
    photo.setCreatedAt(new Date());
    photo.setUpdatedAt(new Date());
    String resourceId = photoRepository.insert(photo).getId();
    accessService.createAccess(userId, resourceId, ResourceType.PHOTO, AccessStatusType.OWNER);

    return photo;
  }

  @Override
  public Metadata addPhotoMetadata(String photoId, Metadata metadata, String userId)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    Photo photo =
        photoRepository
            .findById(photoId)
            .orElseThrow(
                () ->
                    new ResourceNotFoundException(
                        String.format("Photo [id: %s] does not exits", photoId)));

    if (!accessService.isOwner(userId, photoId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] cannot add metadata to photo [id: %s]", userId, photoId));
    metadata.setCreatedAt(new Date());
    metadata.setUpdatedAt(new Date());
    Metadata entity = metadataRepository.save(metadata);
    photo.setMetadata(entity);

    return photoRepository.save(photo).getMetadata();
  }

  @Override
  public Metadata updatePhotoMetadata(String photoId, Metadata metadata, String userId)
      throws IllegalResourceAccessException {
    Photo photo =
        photoRepository
            .findById(photoId)
            .orElseThrow(
                () ->
                    new ResourceNotFoundException(
                        String.format("Photo [id: %s] does not exits", photoId)));

    if (!accessService.canUpdate(userId, photoId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] cannot update photo metadata [id: %s]", userId, photoId));
    if (photo.getMetadata() == null)
      throw new RuntimeException(String.format("Metadata of photo %s is null", photoId));
    photo.getMetadata().setTitle(metadata.getTitle());
    photo.getMetadata().setDescription(metadata.getDescription());
    photo.getMetadata().setTags(metadata.getTags());
    photo.getMetadata().setUpdatedAt(new Date());
    return photoRepository
        .save(photo.setMetadata(metadataRepository.save(photo.getMetadata())))
        .getMetadata();
  }

  @Override
  public void deletePhotoById(String photoId, String userId) {
    Photo photo =
        photoRepository
            .findById(photoId)
            .orElseThrow(
                () ->
                    new ResourceNotFoundException(
                        String.format("Photo [id: %s] does not exits", photoId)));

    if (!accessService.canDelete(userId, photoId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] cannot delete photo [id: %s]", userId, photoId));
    if (photo != null && photo.getMetadata() != null) {
      String gridFsPictureId = photo.getGridfsPictureId();
      metadataRepository.deleteById(photo.getMetadata().getId());
      photoRepository.deleteById(photoId);
      gridFsTemplate.delete(new Query(Criteria.where("_id").is(gridFsPictureId)));
      accessService.deleteAllAccessByResource(photoId);
    }
  }
}
