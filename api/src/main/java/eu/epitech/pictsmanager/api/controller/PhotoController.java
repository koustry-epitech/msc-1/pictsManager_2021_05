package eu.epitech.pictsmanager.api.controller;

import eu.epitech.pictsmanager.api.controller.api.dto.MetadataDto;
import eu.epitech.pictsmanager.api.controller.api.dto.PhotoDto;
import eu.epitech.pictsmanager.api.exception.IllegalResourceAccessException;
import eu.epitech.pictsmanager.api.exception.ResourceNotFoundException;
import eu.epitech.pictsmanager.api.model.Metadata;
import eu.epitech.pictsmanager.api.model.Photo;
import eu.epitech.pictsmanager.api.service.PhotoService;
import eu.epitech.pictsmanager.api.utils.MetadataMapperUtils;
import eu.epitech.pictsmanager.api.utils.PhotoMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.List;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
@Slf4j
@RestController()
@RequestMapping("photos")
public class PhotoController {

  private final PhotoService photoService;

  /**
   * Instantiates a new Photo controller.
   *
   * @param photoService the photo service
   */
  @Autowired
  public PhotoController(PhotoService photoService) {
    this.photoService = photoService;
  }

  /**
   * Gets all photos ids.
   *
   * @param principal the principal
   * @return the all photos ids
   */
  @GetMapping(value = "getAllIds", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<String> getAllPhotosIds(@AuthenticationPrincipal Principal principal) {
    log.info("Retrieving all photo ids for user {}", principal.getName());
    return photoService.getAllPhotosIds(principal.getName());
  }

  /**
   * Gets photo data by id.
   *
   * @param id the id
   * @param principal the principal
   * @return the photo data by id
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  @GetMapping(value = "{id}/data", produces = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<?> getPhotoDataById(
      @PathVariable String id, @AuthenticationPrincipal Principal principal)
      throws IllegalResourceAccessException {
    log.info("Retrieving photo data for photo {} and user {}", id, principal.getName());
    Photo photo = photoService.getPhotoById(id, principal.getName()).orElse(null);

    if (photo != null) {
      Long contentLength = photo.getSize();
      InputStream photoInputStream = photoService.getPhotoData(photo);
      InputStreamResource inputStreamResource = new InputStreamResource(photoInputStream);
      return ResponseEntity.ok()
          .header("Content-Length", contentLength.toString())
          .contentType(MediaType.IMAGE_JPEG)
          .body(inputStreamResource);
    }
    return new ResponseEntity<>("Photo not found".getBytes(), HttpStatus.NOT_FOUND);
  }

  /**
   * Gets photo metadata by id.
   *
   * @param id the id
   * @param principal the principal
   * @return the photo metadata by id
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  @GetMapping("{id}/metadata")
  public ResponseEntity<MetadataDto> getPhotoMetadataById(
      @PathVariable String id, @AuthenticationPrincipal Principal principal)
      throws IllegalResourceAccessException {
    log.info("Retrieving photo metadata for photo {} and user {}", id, principal.getName());
    Metadata metadata = photoService.getPhotoMetadataByUserAccess(id, principal.getName());
    MetadataDto dto = MetadataMapperUtils.convertToDto(metadata);

    return new ResponseEntity<>(dto, HttpStatus.OK);
  }

  /**
   * Add photo data response entity.
   *
   * @param request the request
   * @return the response entity
   * @throws IOException the io exception
   */
  @PostMapping(value = "data", consumes = MediaType.IMAGE_JPEG_VALUE)
  public ResponseEntity<PhotoDto> addPhotoData(HttpServletRequest request) throws IOException {
    String userId = request.getUserPrincipal().getName();
    log.info("Adding photo data by user {}", userId);
    Long contentLength = Long.parseLong(request.getHeader("Content-Length"));
    Photo photo = photoService.addPhoto(contentLength, request.getInputStream(), userId);
    PhotoDto dto = PhotoMapperUtils.convertToDto(photo);

    return new ResponseEntity<>(dto, HttpStatus.CREATED);
  }

  /**
   * Add photo metadata response entity.
   *
   * @param principal the principal
   * @param photoId the photo id
   * @param metadataDto the metadata dto
   * @return the response entity
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  @PostMapping("{photoId}/metadata")
  public ResponseEntity<MetadataDto> addPhotoMetadata(
      @AuthenticationPrincipal Principal principal,
      @PathVariable String photoId,
      @RequestBody MetadataDto metadataDto)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    log.info("Adding photo metadata for photo {} by user {}", photoId, principal.getName());
    Metadata metadata =
        photoService.addPhotoMetadata(
            photoId, MetadataMapperUtils.convertToEntity(metadataDto), principal.getName());
    return new ResponseEntity<>(MetadataMapperUtils.convertToDto(metadata), HttpStatus.CREATED);
  }

  /**
   * Update photo metadata response entity.
   *
   * @param principal the principal
   * @param photoId the photo id
   * @param metadataDto the metadata dto
   * @return the response entity
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  @PutMapping("{photoId}/metadata")
  public ResponseEntity<MetadataDto> updatePhotoMetadata(
      @AuthenticationPrincipal Principal principal,
      @PathVariable String photoId,
      @RequestBody MetadataDto metadataDto)
      throws IllegalResourceAccessException {
    log.info("Updating photo data for photo {} by user {}", photoId, principal.getName());
    Metadata entity =
        photoService.updatePhotoMetadata(
            photoId, MetadataMapperUtils.convertToEntity(metadataDto), principal.getName());
    MetadataDto dto = MetadataMapperUtils.convertToDto(entity);

    return new ResponseEntity<>(dto, HttpStatus.OK);
  }

  /**
   * Delete photo.
   *
   * @param id the id
   * @param principal the principal
   */
  @DeleteMapping("{id}")
  public void deletePhoto(@PathVariable String id, @AuthenticationPrincipal Principal principal) {
    log.info("Deleting photo {} by user {}", id, principal.getName());
    photoService.deletePhotoById(id, principal.getName());
  }
}
