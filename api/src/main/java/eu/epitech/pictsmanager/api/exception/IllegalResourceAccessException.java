package eu.epitech.pictsmanager.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 23/06/2020
 */
@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Access to the resource not allowed")
public class IllegalResourceAccessException extends RuntimeException {
  public IllegalResourceAccessException(String message) {
    super(message);
  }
}
