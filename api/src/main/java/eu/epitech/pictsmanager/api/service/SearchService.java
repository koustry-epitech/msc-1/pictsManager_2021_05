package eu.epitech.pictsmanager.api.service;

import eu.epitech.pictsmanager.api.controller.api.dto.SearchResultDto;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author Lucas Fabre
 * @since 09 /06/2020
 */
public interface SearchService {

  /**
   * Search search result dto.
   *
   * @param query the query
   * @param userId the user id
   * @return the search result dto
   */
  SearchResultDto search(String query, String userId);
}
