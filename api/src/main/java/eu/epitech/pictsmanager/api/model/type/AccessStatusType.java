package eu.epitech.pictsmanager.api.model.type;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 06/06/2020
 */
public enum AccessStatusType {
    OWNER,
    READER
}
