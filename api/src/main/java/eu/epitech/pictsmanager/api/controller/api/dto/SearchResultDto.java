package eu.epitech.pictsmanager.api.controller.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author Lucas Fabre
 * @since 13/06/2020
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper=false)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class SearchResultDto {

    private List<PhotoDto> photos;
    private List<AlbumDto> albums;

}
