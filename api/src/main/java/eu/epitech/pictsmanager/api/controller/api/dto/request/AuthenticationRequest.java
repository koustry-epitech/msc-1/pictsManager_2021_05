package eu.epitech.pictsmanager.api.controller.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 15 /06/2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequest {

  @NotNull(message = "username cannot be null")
  @NotEmpty(message = "username cannot be empty")
  private String username;

  @NotNull(message = "password cannot be null")
  @NotEmpty(message = "password cannot be empty")
  private String password;
}
