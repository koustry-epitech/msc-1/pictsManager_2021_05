package eu.epitech.pictsmanager.api.utils;

import eu.epitech.pictsmanager.api.controller.api.dto.MetadataDto;
import eu.epitech.pictsmanager.api.model.Metadata;
import org.modelmapper.ModelMapper;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
public class MetadataMapperUtils {

  /**
   * Convert to dto metadata dto.
   *
   * @param entity the entity
   * @return the metadata dto
   */
  public static MetadataDto convertToDto(Metadata entity) {
    return new ModelMapper().map(entity, MetadataDto.class);
  }

  /**
   * Convert to entity metadata.
   *
   * @param dto the dto
   * @return the metadata
   */
  public static Metadata convertToEntity(MetadataDto dto) {
    return new ModelMapper().map(dto, Metadata.class);
  }
}
