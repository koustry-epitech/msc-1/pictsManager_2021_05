package eu.epitech.pictsmanager.api.controller.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 30 /05/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class UserDto {

  private String id;

  private String username;

  private String password;

  /**
   * Instantiates a new User dto.
   *
   * @param id the id
   * @param username the username
   */
  public UserDto(String id, String username) {}
}
