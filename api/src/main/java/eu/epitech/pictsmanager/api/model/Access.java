package eu.epitech.pictsmanager.api.model;

import eu.epitech.pictsmanager.api.model.type.AccessStatusType;
import eu.epitech.pictsmanager.api.model.type.ResourceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 06 /06/2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Document(collection = "access")
public class Access extends AbstractBaseModel {

  @NotNull private String userId;

  @NotNull private String resourceId;

  @NotNull private ResourceType resourceType;

  @NotNull private AccessStatusType accessStatus;
}
