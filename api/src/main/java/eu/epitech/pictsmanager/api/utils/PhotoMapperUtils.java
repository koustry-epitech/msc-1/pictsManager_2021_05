package eu.epitech.pictsmanager.api.utils;

import eu.epitech.pictsmanager.api.controller.api.dto.PhotoDto;
import eu.epitech.pictsmanager.api.model.Photo;
import org.modelmapper.ModelMapper;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
public class PhotoMapperUtils {

  /**
   * Convert to dto photo dto.
   *
   * @param entity the entity
   * @return the photo dto
   */
  public static PhotoDto convertToDto(Photo entity) {
    return new ModelMapper().map(entity, PhotoDto.class);
  }

  /**
   * Convert to entity photo.
   *
   * @param dto the dto
   * @return the photo
   */
  public static Photo convertToEntity(PhotoDto dto) {
    return new ModelMapper().map(dto, Photo.class);
  }
}
