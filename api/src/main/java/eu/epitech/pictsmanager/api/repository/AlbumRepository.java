package eu.epitech.pictsmanager.api.repository;

import eu.epitech.pictsmanager.api.model.Album;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 02 /06/2020
 */
public interface AlbumRepository extends MongoRepository<Album, String> {

  @Query("{'$or':[{ 'metadata.title' : { '$regex' : '?0', '$options' : 'i' }}, { 'metadata.description' : { '$regex' : '?0', '$options' : 'i' }}, { 'metadata.tags' : { '$regex' : '?0', '$options' : 'i' }}]}")
  List<Album> findAllByFreeTextSearch(String keyword);

}
