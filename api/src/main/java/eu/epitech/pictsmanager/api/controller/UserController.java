package eu.epitech.pictsmanager.api.controller;

import eu.epitech.pictsmanager.api.controller.api.dto.UserDto;
import eu.epitech.pictsmanager.api.model.User;
import eu.epitech.pictsmanager.api.service.UserService;
import eu.epitech.pictsmanager.api.utils.UserMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/** The type User controller. */
@RestController
@RequestMapping("/users")
public class UserController {

  private UserService userService;

  /**
   * Instantiates a new User controller.
   *
   * @param userService the user service
   */
  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * Gets all users.
   *
   * @return the all users
   */
  @GetMapping
  @Secured({"ROLE_ADMIN"})
  public ResponseEntity<List<UserDto>> getAllUsers() {
    List<User> users = userService.getAllUsers();
    List<UserDto> usersList =
        users.stream()
            .map(
                u -> {
                  u.setPassword(null);
                  return UserMapperUtils.convertToDto(u);
                })
            .collect(Collectors.toList());

    return new ResponseEntity<>(usersList, HttpStatus.OK);
  }

  /**
   * Gets user.
   *
   * @param id the id
   * @return the user
   */
  @GetMapping("{id}")
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  public ResponseEntity<UserDto> getUser(@PathVariable("id") String id) {
    User user = userService.getUserById(id);
    UserDto dto = UserMapperUtils.convertToDto(user);

    dto.setPassword(null);
    return new ResponseEntity<>(dto, HttpStatus.OK);
  }

  /**
   * Create user response entity.
   *
   * @param userDTO the user dto
   * @return the response entity
   */
  @PostMapping
  @Secured({"ROLE_ADMIN"})
  public ResponseEntity<?> createUser(@RequestBody @Valid UserDto userDTO) {
    try {
      User user = userService.createUserFromDTO(userDTO);
      UserDto dto = new UserDto(user.getId(), user.getUsername());
      return ResponseEntity.created(URI.create(String.format("/users/%s", dto.getId()))).body(dto);
    } catch (RuntimeException e) {
      return ResponseEntity.badRequest().body(Map.of("Error", e.getMessage()));
    }
  }

  /**
   * Update user response entity.
   *
   * @param id the id
   * @param userDto the user dto
   * @return the response entity
   */
  @PutMapping("{id}")
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  public ResponseEntity<User> updateUser(
      @PathVariable("id") String id, @RequestBody @Valid UserDto userDto) {
    User u = userService.updateUserFromDto(id, userDto);
    //    UserDto dto =
    //        new
    // UserDto().setId(u.getId()).setUsername(u.getUsername()).setPassword(u.getPassword());

    return new ResponseEntity<>(u, HttpStatus.OK);
  }

  /**
   * Delete user response entity.
   *
   * @param id the id
   * @return the response entity
   */
  @DeleteMapping("{id}")
  @Secured({"ROLE_ADMIN"})
  public ResponseEntity<Void> deleteUser(@PathVariable("id") String id) {
    userService.deleteUserById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
