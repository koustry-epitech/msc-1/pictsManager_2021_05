package eu.epitech.pictsmanager.api.repository;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import eu.epitech.pictsmanager.api.model.Photo;

public class CustomPhotoRepositoryImpl implements CustomPhotoRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<String> findAllPhotosIds() {
        Query query = new Query();
        query.fields().include("_id");
        return mongoTemplate.find(query, Photo.class).stream()
            .map(p -> p.getId())
            .collect(Collectors.toList());
    }
}

