package eu.epitech.pictsmanager.api.init;

import com.google.common.collect.Sets;
import eu.epitech.pictsmanager.api.model.Role;
import eu.epitech.pictsmanager.api.model.User;
import eu.epitech.pictsmanager.api.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 29/06/2020
 */
@Slf4j
@Component
public class FirstUserInit {

  @Autowired private UserRepository userRepository;

  @Autowired private PasswordEncoder passwordEncoder;

  @PostConstruct
  private void postConstruct() {
    if (userRepository.existsByUsername("admin")) {
      log.error("User admin already exists, stop init first user");
      return;
    }
    User admin = new User();
    admin
        .setUsername("admin")
        .setPassword(passwordEncoder.encode("admin"))
        .setEnabled(true)
        .setAccountNonLocked(true)
        .setAccountNonExpired(true)
        .setCredentialsNonExpired(true)
        .setGrantedAuthorities(
            Sets.newHashSet(new SimpleGrantedAuthority(Role.ROLE_ADMIN.toString())));
    log.info("Created initial admin user {}", userRepository.save(admin).getId());

    if (userRepository.existsByUsername("user")) {
      log.error("User user already exists, stop init first user");
      return;
    }
    User user = new User();
    user
      .setUsername("user")
      .setPassword(passwordEncoder.encode("user"))
      .setEnabled(true)
      .setAccountNonLocked(true)
      .setAccountNonExpired(true)
      .setCredentialsNonExpired(true)
      .setGrantedAuthorities(
        Sets.newHashSet(new SimpleGrantedAuthority(Role.ROLE_USER.toString())));
    log.info("Created initial normal user {}", userRepository.save(user).getId());
  }
}
