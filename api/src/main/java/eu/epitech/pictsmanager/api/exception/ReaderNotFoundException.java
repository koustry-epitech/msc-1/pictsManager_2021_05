package eu.epitech.pictsmanager.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 30 /06/2020
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "User not found")
public class ReaderNotFoundException extends RuntimeException {
  /**
   * Instantiates a new Reader not found exception.
   *
   * @param message the message
   */
  public ReaderNotFoundException(String message) {
    super(message);
  }
}
