package eu.epitech.pictsmanager.api.service.impl;

import eu.epitech.pictsmanager.api.exception.AccessAlreadyExists;
import eu.epitech.pictsmanager.api.exception.IllegalResourceAccessException;
import eu.epitech.pictsmanager.api.exception.ReaderNotFoundException;
import eu.epitech.pictsmanager.api.exception.ResourceNotFoundException;
import eu.epitech.pictsmanager.api.model.Access;
import eu.epitech.pictsmanager.api.model.Album;
import eu.epitech.pictsmanager.api.model.User;
import eu.epitech.pictsmanager.api.model.type.AccessStatusType;
import eu.epitech.pictsmanager.api.model.type.ResourceType;
import eu.epitech.pictsmanager.api.repository.AccessRepository;
import eu.epitech.pictsmanager.api.repository.AlbumRepository;
import eu.epitech.pictsmanager.api.repository.PhotoRepository;
import eu.epitech.pictsmanager.api.repository.UserRepository;
import eu.epitech.pictsmanager.api.service.AccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 06 /06/2020
 */
@Service
public class AccessServiceImpl implements AccessService {

  private final AccessRepository accessRepository;
  private final AlbumRepository albumRepository;
  private final PhotoRepository photoRepository;
  private final UserRepository userRepository;

  /**
   * Instantiates a new Access service.
   *
   * @param accessRepository the access repository
   * @param albumRepository the album repository
   * @param photoRepository the photo repository
   * @param userRepository the user repository
   */
  @Autowired
  public AccessServiceImpl(
      AccessRepository accessRepository,
      AlbumRepository albumRepository,
      PhotoRepository photoRepository,
      UserRepository userRepository) {
    this.accessRepository = accessRepository;
    this.albumRepository = albumRepository;
    this.photoRepository = photoRepository;
    this.userRepository = userRepository;
  }

  @Override
  public List<Access> loadAccessByUser(String userId) {
    return accessRepository.findAllByUserId(userId);
  }

  @Override
  public List<Access> loadAccessByResource(String resourceId, String userId) {
    return accessRepository.findAllByResourceId(resourceId).stream()
        .filter(access -> isOwner(userId, resourceId))
        .collect(Collectors.toList());
  }

  @Override
  public Access createAccess(
      String userId, String resourceId, ResourceType resourceType, AccessStatusType accessType) {
    if (accessRepository.existsByUserIdAndResourceId(userId, resourceId))
      throw new AccessAlreadyExists(String.format("The access for Reader [id: %s] and Resource [id: %s] already exists", userId, resourceId));

    Access access = new Access();

    access.setUserId(userId);
    access.setResourceId(resourceId);
    access.setResourceType(resourceType);
    access.setAccessStatus(accessType);
    access.setCreatedAt(new Date());
    access.setUpdatedAt(new Date());

    return accessRepository.save(access);
  }

  @Override
  public void deleteAllAccessByResource(String resourceId) {
    List<Access> accessList = accessRepository.findAllByResourceId(resourceId);

    for (Access access : accessList) accessRepository.deleteById(access.getId());
  }

  @Override
  public boolean canRead(String userId, String resourceId) {
    Access access = accessRepository.findByUserIdAndResourceId(userId, resourceId);
    return Objects.nonNull(access)
        && (access.getAccessStatus().equals(AccessStatusType.OWNER)
            || access.getAccessStatus().equals(AccessStatusType.READER));
  }

  @Override
  public boolean canUpdate(String userId, String resourceId) {
    if (accessRepository.existsByUserIdAndResourceId(userId, resourceId)) {
      Access access = accessRepository.findByUserIdAndResourceId(userId, resourceId);
      return Objects.nonNull(access) && access.getAccessStatus().equals(AccessStatusType.OWNER);
    }
    return false;
  }

  @Override
  public boolean canDelete(String userId, String resourceId) {
    return canUpdate(userId, resourceId);
  }

  @Override
  public boolean isOwner(String userId, String resourceId) {
    Access access = accessRepository.findByUserIdAndResourceId(userId, resourceId);
    return access != null && access.getAccessStatus().equals(AccessStatusType.OWNER);
  }

  @Override
  public boolean isOwner(String userId, List<String> resourceIdList) {
    for (String resourceId : resourceIdList) if (!isOwner(userId, resourceId)) return false;
    return true;
  }

  @Override
  public void createReadersAccessToAlbum(String albumId, List<String> readersList, String ownerId)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    Album album = getAlbumAndCheckOwnership(albumId, ownerId);
    User owner = userRepository.findById(ownerId).orElseThrow(() -> new RuntimeException("Owner not found"));

    Objects.requireNonNull(album.getPhotoIdsList());
    for (String username : readersList) {
      User user = userRepository.findUserByUsername(username).orElseThrow(() -> new ReaderNotFoundException(String.format("Reader [username: %s] does not exits", username)));
      if (owner.getUsername().equals(user.getUsername()))
        throw new RuntimeException("The user is already OWNER");
      createAccess(user.getId(), albumId, ResourceType.ALBUM, AccessStatusType.READER);
      for (String photoId : album.getPhotoIdsList()) {
        // Double check because Album Service already check if the user is owner of both album and
        // photo before adding photo to album
        if (photoRepository.existsById(photoId) && isOwner(ownerId, photoId))
          createAccess(user.getId(), photoId, ResourceType.PHOTO, AccessStatusType.READER);
      }
    }
  }

  @Override
  public void deleteReadersAccessFromAlbum(String albumId, List<String> readersList, String ownerId)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    Album album = getAlbumAndCheckOwnership(albumId, ownerId);

    Objects.requireNonNull(album.getPhotoIdsList());
    for (String username : readersList) {
      User user = userRepository.findUserByUsername(username).orElseThrow(() -> new UsernameNotFoundException(""));
      accessRepository.deleteByUserIdAndResourceId(user.getId(), albumId);
      for (String photoId : album.getPhotoIdsList())
        if (photoRepository.existsById(photoId) && isOwner(ownerId, photoId))
          accessRepository.deleteByUserIdAndResourceId(user.getId(), photoId);
    }
  }

  @Override
  public void createReadersAccessToPhoto(String photoId, List<String> readersList, String ownerId)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    if (!photoRepository.existsById(photoId))
      throw new ResourceNotFoundException(String.format("Album [id: %s] was not found", photoId));
    if (!isOwner(ownerId, photoId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] is not the owner of photo [id: %s]", ownerId, photoId));
    for (String username : readersList) {
      User user = userRepository.findUserByUsername(username).orElseThrow(() -> new ReaderNotFoundException(String.format("Reader [username: %s] does not exits", username)));
      if (user.getId().equals(ownerId))
        throw new RuntimeException("The user is already OWNER");
      createAccess(user.getId(), photoId, ResourceType.PHOTO, AccessStatusType.READER);
    }
  }

  @Override
  public void deleteReadersAccessFromPhoto(String photoId, List<String> readersList, String ownerId)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    if (!photoRepository.existsById(photoId))
      throw new ResourceNotFoundException(String.format("Album [id: %s] was not found", photoId));
    if (!isOwner(ownerId, photoId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] is not the owner of photo [id: %s]", ownerId, photoId));
    for (String username : readersList) {
      User user = userRepository.findUserByUsername(username).orElseThrow(() -> new ReaderNotFoundException(String.format("Reader [username: %s] does not exits", username)));
      accessRepository.deleteByUserIdAndResourceId(user.getId(), photoId);
    }
  }

  private Album getAlbumAndCheckOwnership(String resourceId, String ownerId) {
    Album album =
        albumRepository
            .findById(resourceId)
            .orElseThrow(
                () ->
                    new ResourceNotFoundException(
                        String.format("Album [id: %s] was not found", resourceId)));

    if (!isOwner(ownerId, resourceId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] is not the owner", ownerId));
    return album;
  }
}
