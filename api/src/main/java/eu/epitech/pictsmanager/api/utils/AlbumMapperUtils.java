package eu.epitech.pictsmanager.api.utils;

import eu.epitech.pictsmanager.api.controller.api.dto.AlbumDto;
import eu.epitech.pictsmanager.api.model.Album;
import org.modelmapper.ModelMapper;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 02/06/2020
 */
public class AlbumMapperUtils {

    public static AlbumDto convertToDto(Album entity) {
        return new ModelMapper().map(entity, AlbumDto.class);
    }

    public static Album convertToEntity(AlbumDto dto) {
        return new ModelMapper().map(dto, Album.class);
    }

}
