package eu.epitech.pictsmanager.api.repository;

import java.util.List;

import eu.epitech.pictsmanager.api.model.Photo;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author Lucas Fabre
 * @since 06/06/2020
 */
public interface CustomPhotoRepository {
    List<String> findAllPhotosIds();
}
