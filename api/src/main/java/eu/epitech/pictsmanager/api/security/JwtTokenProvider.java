package eu.epitech.pictsmanager.api.security;

import eu.epitech.pictsmanager.api.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Calendar;
import java.util.Date;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 15 /06/2020
 */
@Component
public class JwtTokenProvider {

  private Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

  @Value(value = "${token.secret}")
  private String secret;

  public String createToken(Authentication authentication) {
    User user = (User) authentication.getPrincipal();
    Key signingKey = new SecretKeySpec(secret.getBytes(), SignatureAlgorithm.HS512.getJcaName());

    String token =
        Jwts.builder()
            .signWith(signingKey, SignatureAlgorithm.HS512)
            .setClaims(buildUserClaims(user))
            .setExpiration(getTokenExpirationDate(false))
            .setIssuedAt(new Date())
            .compact();

    logger.info("Token created {}", token);
    return token;
  }

  public Jws<Claims> validateJwtToken(String token) {
    Key signingKey = new SecretKeySpec(secret.getBytes(), SignatureAlgorithm.HS512.getJcaName());
    Jws<Claims> claims =
        Jwts.parserBuilder().setSigningKey(signingKey).build().parseClaimsJws(token);

    logger.info("Validate JWT claims : {}", claims);
    return claims;
  }

  private Claims buildUserClaims(User user) {
    Claims claims = new DefaultClaims();

    claims.setSubject(user.getId());
    claims.put("username", user.getUsername());
    claims.put("roles", String.join(",", AuthorityUtils.authorityListToSet(user.getAuthorities())));
    logger.info("Create claims {}", claims);
    return claims;
  }

  private Date getTokenExpirationDate(boolean refreshToken) {
    Calendar calendar = Calendar.getInstance();

    if (refreshToken) calendar.add(Calendar.MONTH, 1);
    else calendar.add(Calendar.MINUTE, 5);
    return calendar.getTime();
  }
}
