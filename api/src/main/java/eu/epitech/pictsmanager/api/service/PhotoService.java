package eu.epitech.pictsmanager.api.service;

import eu.epitech.pictsmanager.api.exception.IllegalResourceAccessException;
import eu.epitech.pictsmanager.api.exception.ResourceNotFoundException;
import eu.epitech.pictsmanager.api.model.Metadata;
import eu.epitech.pictsmanager.api.model.Photo;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
public interface PhotoService {

  /**
   * Gets all photos.
   *
   * @return the all photos
   */
  @Deprecated
  List<Photo> getAllPhotos();

  /**
   * Gets all photos ids.
   *
   * @return the all photos ids
   */
  @Deprecated
  List<String> getAllPhotosIds();

  /**
   * Gets all photos ids.
   *
   * @param userId the user id
   * @return the all photos ids
   */
  List<String> getAllPhotosIds(String userId);

  /**
   * Gets photo by id.
   *
   * @param photoId the photo id
   * @param userId the user id
   * @return the photo by id
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  Optional<Photo> getPhotoById(String photoId, String userId) throws IllegalResourceAccessException;

  /**
   * Gets photo data.
   *
   * @param photo the photo
   * @return the photo data
   */
  InputStream getPhotoData(Photo photo);

  /**
   * Gets photo metadata.
   *
   * @param photoId the photo id
   * @return the photo metadata
   */
  Metadata getPhotoMetadata(String photoId);

  /**
   * Gets photo metadata by user access.
   *
   * @param photoId the photo id
   * @param userId the user id
   * @return the photo metadata by user access
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  Metadata getPhotoMetadataByUserAccess(String photoId, String userId)
      throws IllegalResourceAccessException;

  /**
   * Add photo photo.
   *
   * @param contentLength the size of the picture
   * @param picture the picture
   * @param userId the user id
   * @return the photo
   * @throws IOException the io exception
   */
  Photo addPhoto(Long contentLength, InputStream picture, String userId) throws IOException;

  /**
   * Add photo metadata metadata.
   *
   * @param photoId the photo id
   * @param metadata the metadata
   * @param userId the user id
   * @return the metadata
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  Metadata addPhotoMetadata(String photoId, Metadata metadata, String userId)
      throws ResourceNotFoundException, IllegalResourceAccessException;

  /**
   * Update photo metadata metadata.
   *
   * @param photoId the photo id
   * @param metadata the metadata
   * @param userId the user id
   * @return the metadata
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  Metadata updatePhotoMetadata(String photoId, Metadata metadata, String userId)
      throws IllegalResourceAccessException;

  /**
   * Delete photo by id.
   *
   * @param photoId the photo id
   * @param userId the user id
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  void deletePhotoById(String photoId, String userId) throws IllegalResourceAccessException;
}
