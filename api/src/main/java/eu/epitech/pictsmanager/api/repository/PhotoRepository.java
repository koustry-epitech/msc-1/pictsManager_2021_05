package eu.epitech.pictsmanager.api.repository;

import eu.epitech.pictsmanager.api.model.Photo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 30 /05/2020
 */
public interface PhotoRepository extends MongoRepository<Photo, String>, CustomPhotoRepository {

  @Query("{'$or':[{ 'metadata.title' : { '$regex' : '?0', '$options' : 'i' }}, { 'metadata.description' : { '$regex' : '?0', '$options' : 'i' }}, { 'metadata.tags' : { '$regex' : '?0', '$options' : 'i' }}]}")
  List<Photo> findAllByFreeTextSearch(String keyword);

}
