package eu.epitech.pictsmanager.api.controller.api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 23/06/2020
 */
@Data
@AllArgsConstructor
public class AuthenticationResponse {

  private final String authenticationType = "Bearer";
  private String accessToken;

}
