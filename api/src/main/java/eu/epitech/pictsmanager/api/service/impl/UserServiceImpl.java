package eu.epitech.pictsmanager.api.service.impl;

import com.google.common.collect.Sets;
import eu.epitech.pictsmanager.api.controller.api.dto.UserDto;
import eu.epitech.pictsmanager.api.model.Role;
import eu.epitech.pictsmanager.api.model.User;
import eu.epitech.pictsmanager.api.repository.UserRepository;
import eu.epitech.pictsmanager.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/** The type User service. */
@Service
public class UserServiceImpl implements UserService, UserDetailsService {

  @Autowired private UserRepository userRepository;

  @Autowired private PasswordEncoder passwordEncoder;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Objects.requireNonNull(username);
    return userRepository
        .findUserByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException("User Not Found"));
  }

  @Override
  public List<User> getAllUsers() {
    return userRepository.findAll();
  }

  @Override
  public User getUserById(String id) {
    Optional<User> user = userRepository.findById(id);

    return user.isPresent() ? user.get() : null;
  }

  @Override
  public User createUserFromDTO(UserDto userDto) throws RuntimeException {
    Optional<User> opt = userRepository.findUserByUsername(userDto.getUsername());
    if (opt.isPresent()) throw new RuntimeException("Username already taken");

    User user =
        new User()
            .setUsername(userDto.getUsername())
            .setPassword(passwordEncoder.encode(userDto.getPassword()))
            .setEnabled(true)
            .setAccountNonLocked(true)
            .setAccountNonExpired(true)
            .setCredentialsNonExpired(true)
            .setGrantedAuthorities(
                Sets.newHashSet(new SimpleGrantedAuthority(Role.ROLE_USER.toString())));
    return userRepository.save(user);
  }

  @Override
  public User updateUserFromDto(final String id, final UserDto userDto) {
    User user = new User();
    Optional<User> res = userRepository.findById(id);

    if (res.isPresent()) {
      res.get().setUsername(userDto.getUsername());
      user = userRepository.save(res.get());
    }
    return user;
  }

  @Override
  public void deleteUserById(String id) {
    userRepository.deleteById(id);
  }

}
