package eu.epitech.pictsmanager.api.controller;

import eu.epitech.pictsmanager.api.exception.IllegalResourceAccessException;
import eu.epitech.pictsmanager.api.exception.ResourceNotFoundException;
import eu.epitech.pictsmanager.api.model.Access;
import eu.epitech.pictsmanager.api.service.AccessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 06 /06/2020
 */
@Slf4j
@RestController
@RequestMapping("access")
public class AccessController {

  private final AccessService accessService;

  /**
   * Instantiates a new Access controller.
   *
   * @param accessService the access service
   */
  @Autowired
  public AccessController(AccessService accessService) {
    this.accessService = accessService;
  }

  /**
   * Gets user access.
   *
   * @param userId the user id
   * @return the user access
   */
  @Secured({"ROLE_ADMIN"})
  @GetMapping(value = "users/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Access> getUserAccess(@PathVariable String userId) {
    return accessService.loadAccessByUser(userId);
  }

  /**
   * Gets resource access.
   *
   * @param resourceId the resource id
   * @param principal the principal
   * @return the resource access
   */
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  @GetMapping(value = "resources/{resourceId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Access> getResourceAccess(
      @PathVariable String resourceId, @AuthenticationPrincipal Principal principal) {
    log.info("Loading resource's access with di {} by user {}", resourceId, principal.getName());
    return accessService.loadAccessByResource(resourceId, principal.getName());
  }

  /**
   * Add album readers.
   *
   * @param albumId the album id
   * @param readersList the readers list
   * @param principal the principal
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  @PostMapping(value = "albums/{albumId}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public void addAlbumReaders(
      @PathVariable String albumId,
      @RequestBody List<String> readersList,
      @AuthenticationPrincipal Principal principal)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    log.info("Adding readers {} to album {} by user {}", readersList, albumId, principal.getName());
    accessService.createReadersAccessToAlbum(albumId, readersList, principal.getName());
  }

  /**
   * Remove album readers.
   *
   * @param albumId the album id
   * @param readersList the readers list
   * @param principal the principal
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  @DeleteMapping(value = "albums/{albumId}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public void removeAlbumReaders(
      @PathVariable String albumId,
      @RequestBody List<String> readersList,
      @AuthenticationPrincipal Principal principal)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    log.info(
        "Deleting readers {} from album {} by user {}", readersList, albumId, principal.getName());
    accessService.deleteReadersAccessFromAlbum(albumId, readersList, principal.getName());
  }

  /**
   * Add photo readers.
   *
   * @param photoId the photo id
   * @param readersList the readers list
   * @param principal the principal
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  @PostMapping(value = "photos/{photoId}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public void addPhotoReaders(
      @PathVariable String photoId,
      @RequestBody List<String> readersList,
      @AuthenticationPrincipal Principal principal)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    log.info("Adding readers {} to photo {} by user {}", readersList, photoId, principal.getName());
    accessService.createReadersAccessToPhoto(photoId, readersList, principal.getName());
  }

  /**
   * Remove photo readers.
   *
   * @param photoId the photo id
   * @param readersList the readers list
   * @param principal the principal
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  @DeleteMapping(value = "photos/{photoId}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public void removePhotoReaders(
      @PathVariable String photoId,
      @RequestBody List<String> readersList,
      @AuthenticationPrincipal Principal principal)
      throws ResourceNotFoundException, IllegalResourceAccessException {
    log.info(
        "Deleting readers {} from photo {} by user {}", readersList, photoId, principal.getName());
    accessService.deleteReadersAccessFromPhoto(photoId, readersList, principal.getName());
  }
}
