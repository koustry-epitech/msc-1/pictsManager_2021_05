package eu.epitech.pictsmanager.api.service.impl;

import eu.epitech.pictsmanager.api.controller.api.dto.AlbumDto;
import eu.epitech.pictsmanager.api.controller.api.dto.PhotoDto;
import eu.epitech.pictsmanager.api.controller.api.dto.SearchResultDto;
import eu.epitech.pictsmanager.api.repository.AlbumRepository;
import eu.epitech.pictsmanager.api.repository.PhotoRepository;
import eu.epitech.pictsmanager.api.service.AccessService;
import eu.epitech.pictsmanager.api.service.SearchService;
import eu.epitech.pictsmanager.api.utils.AlbumMapperUtils;
import eu.epitech.pictsmanager.api.utils.PhotoMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author Lucas Fabre
 * @since 09 /06/2020
 */
@Service
public class SearchServiceImpl implements SearchService {

  private final PhotoRepository photoRepository;
  private final AlbumRepository albumRepository;
  private final AccessService accessService;

  /**
   * Instantiates a new Search service.
   *
   * @param photoRepository the photo repository
   * @param albumRepository the album repository
   * @param accessService the access service
   */
  @Autowired
  public SearchServiceImpl(
      PhotoRepository photoRepository,
      AlbumRepository albumRepository,
      AccessService accessService) {
    this.photoRepository = photoRepository;
    this.albumRepository = albumRepository;
    this.accessService = accessService;
  }

  @Override
  public SearchResultDto search(String query, String userId) {
    SearchResultDto res = new SearchResultDto();
    List<AlbumDto> albums =
        albumRepository.findAllByFreeTextSearch(query).stream()
            .filter(album -> accessService.canRead(userId, album.getId()))
            .map(AlbumMapperUtils::convertToDto)
            .collect(Collectors.toList());
    List<PhotoDto> photos =
        photoRepository.findAllByFreeTextSearch(query).stream()
            .filter(photo -> accessService.canRead(userId, photo.getId()))
            .map(PhotoMapperUtils::convertToDto)
            .collect(Collectors.toList());

    return res.setAlbums(albums).setPhotos(photos);
  }
}
