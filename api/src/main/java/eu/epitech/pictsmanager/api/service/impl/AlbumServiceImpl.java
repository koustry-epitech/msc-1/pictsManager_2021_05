package eu.epitech.pictsmanager.api.service.impl;

import com.google.common.collect.Lists;
import eu.epitech.pictsmanager.api.exception.IllegalResourceAccessException;
import eu.epitech.pictsmanager.api.exception.ResourceNotFoundException;
import eu.epitech.pictsmanager.api.model.Album;
import eu.epitech.pictsmanager.api.model.Metadata;
import eu.epitech.pictsmanager.api.model.type.AccessStatusType;
import eu.epitech.pictsmanager.api.model.type.ResourceType;
import eu.epitech.pictsmanager.api.repository.AlbumRepository;
import eu.epitech.pictsmanager.api.repository.PhotoRepository;
import eu.epitech.pictsmanager.api.service.AccessService;
import eu.epitech.pictsmanager.api.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 02 /06/2020
 */
@Service
public class AlbumServiceImpl implements AlbumService {

  private final AlbumRepository albumRepository;
  private final PhotoRepository photoRepository;
  private final AccessService accessService;

  /**
   * Instantiates a new Album service.
   *
   * @param albumRepository the album repository
   * @param photoRepository the photo repository
   * @param accessService the access service
   */
  @Autowired
  public AlbumServiceImpl(
      AlbumRepository albumRepository,
      PhotoRepository photoRepository,
      AccessService accessService) {
    this.albumRepository = albumRepository;
    this.photoRepository = photoRepository;
    this.accessService = accessService;
  }

  @Override
  @Deprecated
  public List<Album> getAllAlbums() {
    return albumRepository.findAll();
  }

  @Override
  public List<Album> getAllAlbums(String userId) {
    return albumRepository.findAll().stream()
        .filter(a -> accessService.canRead(userId, a.getId()))
        .collect(Collectors.toList());
  }

  @Override
  @Deprecated
  public Album getAlbumById(String id) {
    return albumRepository
        .findById(id)
        .orElseThrow(
            () ->
                new ResourceNotFoundException(String.format("Album with id {} was not found", id)));
  }

  @Override
  public Album getAlbumById(String albumId, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException {
    if (!albumRepository.existsById(albumId))
      throw new ResourceNotFoundException(String.format("Album [id: %s] was not found", albumId));
    if (!accessService.canRead(userId, albumId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] cannot read album [id: %s]", userId, albumId));
    return albumRepository
        .findById(albumId)
        .orElseThrow(
            () ->
                new ResourceNotFoundException(
                    String.format("Album with id {} was not found", albumId)));
  }

  @Override
  public Album createAlbum(Album album, String userId) throws ResourceNotFoundException {
    if (album.getPhotoIdsList() != null) {
      for (String id : album.getPhotoIdsList()) {
        if (!photoRepository.existsById(id))
          throw new ResourceNotFoundException(
              String.format("Photo with id %s does not exists", id));
      }
      if (!accessService.isOwner(userId, Lists.newArrayList(album.getPhotoIdsList())))
        throw new IllegalResourceAccessException(
            String.format(
                "User [id: %s] is not Owner of all photos %s", userId, album.getPhotoIdsList()));
    }
    Album entity = albumRepository.save(album);
    accessService.createAccess(userId, entity.getId(), ResourceType.ALBUM, AccessStatusType.OWNER);
    return entity;
  }

  @Override
  @Deprecated
  public Album updateAlbumMetadata(String id, Metadata metadata) {
    Album album =
        albumRepository.findById(id).orElseThrow(() -> new RuntimeException("Album Not Found"));

    album.getMetadata().setTitle(metadata.getTitle());
    album.getMetadata().setDescription(metadata.getDescription());
    album.getMetadata().setTags(metadata.getTags());
    return albumRepository.save(album);
  }

  @Override
  public Album updateAlbumMetadata(String albumId, Metadata metadata, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException {
    if (!accessService.canUpdate(userId, albumId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] cannot update album metadata [id: %s]", userId, albumId));

    Album album =
        albumRepository
            .findById(albumId)
            .orElseThrow(
                () ->
                    new ResourceNotFoundException(
                        String.format("Album [id: %s] was not found", albumId)));
    album.setUpdatedAt(new Date());
    album
        .getMetadata()
        .setTitle(metadata.getTitle())
        .setDescription(metadata.getDescription())
        .setTags(metadata.getTags())
        .setUpdatedAt(new Date());
    return albumRepository.save(album);
  }

  @Override
  @Deprecated
  public Album addPhotosToAlbum(String albumId, List<String> photosId) {
    Album album =
        albumRepository
            .findById(albumId)
            .orElseThrow(() -> new RuntimeException("Album Not Found"));

    for (String id : photosId) {
      photoRepository
          .findById(id)
          .orElseThrow(() -> new RuntimeException("This Photo Does Not Exists " + id));
      album.getPhotoIdsList().add(id);
    }
    return albumRepository.save(album);
  }

  @Override
  public Album addPhotosToAlbum(String albumId, List<String> photoIdsList, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException {
    Album album =
        albumRepository
            .findById(albumId)
            .orElseThrow(
                () ->
                    new ResourceNotFoundException(
                        String.format("Album [id: %s] does not exits", albumId)));

    checkResourcesExistAndAccess(albumId, photoIdsList, userId);
    album.getPhotoIdsList().addAll(photoIdsList);
    album.setUpdatedAt(new Date());
    return albumRepository.save(album);
  }

  @Override
  @Deprecated
  public Album removePhotosFromAlbum(String albumId, List<String> photosId)
      throws ResourceNotFoundException {
    Album album =
        albumRepository
            .findById(albumId)
            .orElseThrow(() -> new ResourceNotFoundException("Album Not Found"));

    album.getPhotoIdsList().removeAll(photosId);
    return albumRepository.save(album);
  }

  @Override
  public Album removePhotosFromAlbum(String albumId, List<String> photoIdsList, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException {
    Album album =
        albumRepository
            .findById(albumId)
            .orElseThrow(
                () ->
                    new ResourceNotFoundException(
                        String.format("Album [id: %s] does not exits", albumId)));

    checkResourcesExistAndAccess(albumId, photoIdsList, userId);
    album.getPhotoIdsList().removeAll(photoIdsList);
    album.setUpdatedAt(new Date());
    return albumRepository.save(album);
  }

  @Override
  public Void deleteAlbum(String albumId, String userId) throws IllegalResourceAccessException {
    if (!accessService.canDelete(userId, albumId))
      throw new IllegalResourceAccessException(
          String.format("User [id: %s] cannot delete album [id: %s]", userId, albumId));
    albumRepository.deleteById(albumId);
    accessService.deleteAllAccessByResource(albumId);
    return null;
  }

  private void checkResourcesExistAndAccess(
      String albumId, List<String> photoIdsList, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException {
    List<String> resourcesToCheck = new ArrayList<>();

    resourcesToCheck.add(albumId);
    resourcesToCheck.addAll(photoIdsList);
    if (!accessService.isOwner(userId, resourcesToCheck))
      throw new IllegalResourceAccessException(
          String.format(
              "User [id: %s] cannot remove photos [ids: %s] from album [id: %s]",
              userId, photoIdsList, albumId));
    for (String photoId : photoIdsList)
      if (!photoRepository.existsById(photoId))
        throw new ResourceNotFoundException(
            String.format("Photo [id: %s] does not exits", photoId));
  }
}
