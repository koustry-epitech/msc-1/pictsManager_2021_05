package eu.epitech.pictsmanager.api.controller;

import eu.epitech.pictsmanager.api.controller.api.dto.SearchResultDto;
import eu.epitech.pictsmanager.api.service.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author Lucas Fabre
 * @since 06 /06/2020
 */
@Slf4j
@RestController()
@RequestMapping("/search")
public class SearchController {

  private SearchService searchService;

  /**
   * Instantiates a new Search controller.
   *
   * @param searchService the search service
   */
  @Autowired
  public SearchController(SearchService searchService) {
    this.searchService = searchService;
  }

  /**
   * Search search result dto.
   *
   * @param query the query
   * @param principal the principal
   * @return the search result dto
   */
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  @GetMapping(value = "{query}", produces = MediaType.APPLICATION_JSON_VALUE)
  public SearchResultDto search(
      @PathVariable("query") String query, @AuthenticationPrincipal Principal principal) {
    log.info("Searching for query=[{}]", query);
    return searchService.search(query, principal.getName());
  }
}
