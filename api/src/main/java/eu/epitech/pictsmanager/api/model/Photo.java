package eu.epitech.pictsmanager.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.bson.types.Binary;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Document(collection = "photos")
public class Photo extends AbstractResource {

  private String gridfsPictureId;
  private Long size;

  @NotNull(message = "metadata cannot be null")
  private Metadata metadata;

  @NotNull(message = "picture cannot be null")
  private Binary data;
}
