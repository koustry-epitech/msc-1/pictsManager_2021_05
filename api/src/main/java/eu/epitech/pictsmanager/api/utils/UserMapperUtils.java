package eu.epitech.pictsmanager.api.utils;

import eu.epitech.pictsmanager.api.controller.api.dto.UserDto;
import eu.epitech.pictsmanager.api.model.User;
import org.modelmapper.ModelMapper;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
public class UserMapperUtils {

  /**
   * Convert to dto user dto.
   *
   * @param user the user
   * @return the user dto
   */
  public static UserDto convertToDto(User user) {
    return new ModelMapper().map(user, UserDto.class);
  }

  /**
   * Convert to entity user.
   *
   * @param dto the dto
   * @return the user
   */
  public static User convertToEntity(UserDto dto) {
    return new ModelMapper().map(dto, User.class);
  }
}
