package eu.epitech.pictsmanager.api.repository;

import eu.epitech.pictsmanager.api.model.Metadata;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
public interface MetadataRepository extends MongoRepository<Metadata, String> {}
