package eu.epitech.pictsmanager.api.repository;

import eu.epitech.pictsmanager.api.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 30 /05/2020
 */
public interface UserRepository extends MongoRepository<User, String> {

  /**
   * Find user by username optional.
   *
   * @param username the username
   * @return the optional
   */
  Optional<User> findUserByUsername(String username);

  boolean existsByUsername(String username);
}
