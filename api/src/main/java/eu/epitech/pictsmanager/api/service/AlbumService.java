package eu.epitech.pictsmanager.api.service;

import eu.epitech.pictsmanager.api.exception.IllegalResourceAccessException;
import eu.epitech.pictsmanager.api.exception.ResourceNotFoundException;
import eu.epitech.pictsmanager.api.model.Album;
import eu.epitech.pictsmanager.api.model.Metadata;

import java.util.List;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 02/06/2020
 */
public interface AlbumService {

  /**
   * Gets all albums.
   *
   * @return the all albums
   */
  @Deprecated
  List<Album> getAllAlbums();

  /**
   * Gets all albums.
   *
   * @param userId the user id
   * @return the all albums
   */
  List<Album> getAllAlbums(String userId);

  /**
   * Gets album by id.
   *
   * @param id the id
   * @return the album by id
   */
  @Deprecated
  Album getAlbumById(String id);

  /**
   * Gets album by id.
   *
   * @param albumId the album id
   * @param userId the user id
   * @return the album by id
   * @throws IllegalResourceAccessException the illegal resource access exception
   * @throws ResourceNotFoundException the resource not found exception
   */
  Album getAlbumById(String albumId, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException;

  /**
   * Create album album.
   *
   * @param album the album
   * @param userId the user id
   * @return the album
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  Album createAlbum(Album album, String userId)
      throws ResourceNotFoundException, IllegalResourceAccessException;

  /**
   * Update album metadata album.
   *
   * @param id the id
   * @param metadata the metadata
   * @return the album
   */
  @Deprecated
  Album updateAlbumMetadata(String id, Metadata metadata);

  /**
   * Update album metadata album.
   *
   * @param albumId the album id
   * @param metadata the metadata
   * @param userId the user id
   * @return the album
   * @throws IllegalResourceAccessException the illegal resource access exception
   * @throws ResourceNotFoundException the resource not found exception
   */
  Album updateAlbumMetadata(String albumId, Metadata metadata, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException;

  /**
   * Add photos to album album.
   *
   * @param albumId the album id
   * @param photoIds the photo ids
   * @return the album
   */
  @Deprecated
  Album addPhotosToAlbum(String albumId, List<String> photoIds);

  /**
   * Add photos to album album.
   *
   * @param albumId the album id
   * @param photoIdsList the photo ids list
   * @param userId the user id
   * @return the album
   * @throws IllegalResourceAccessException the illegal resource access exception
   * @throws ResourceNotFoundException the resource not found exception
   */
  Album addPhotosToAlbum(String albumId, List<String> photoIdsList, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException;

  /**
   * Remove photos from album album.
   *
   * @param albumId the album id
   * @param photosId the photos id
   * @return the album
   * @throws ResourceNotFoundException the resource not found exception
   */
  @Deprecated
  Album removePhotosFromAlbum(String albumId, List<String> photosId)
      throws ResourceNotFoundException;

  /**
   * Remove photos from album album.
   *
   * @param albumId the album id
   * @param photoIdsList the photo ids list
   * @param userId the user id
   * @return the album
   * @throws IllegalResourceAccessException the illegal resource access exception
   * @throws ResourceNotFoundException the resource not found exception
   */
  Album removePhotosFromAlbum(String albumId, List<String> photoIdsList, String userId)
      throws IllegalResourceAccessException, ResourceNotFoundException;

  /**
   * Delete album void.
   *
   * @param albumId the album id
   * @param userId the user id
   * @return the void
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  Void deleteAlbum(String albumId, String userId) throws IllegalResourceAccessException;
}
