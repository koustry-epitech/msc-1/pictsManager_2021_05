package eu.epitech.pictsmanager.api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.epitech.pictsmanager.api.controller.api.dto.request.AuthenticationRequest;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 15 /06/2020
 */
@Component
public class AuthenticationProvider {

  /** The Object mapper. */
  ObjectMapper objectMapper;
  /** The Authentication manager. */
  AuthenticationManager authenticationManager;

  /**
   * Instantiates a new Authentication service.
   *
   * @param objectMapper the object mapper
   * @param authenticationManager the authentication manager
   */
  @Autowired
  public AuthenticationProvider(
      ObjectMapper objectMapper, AuthenticationManager authenticationManager) {
    this.objectMapper = objectMapper;
    this.authenticationManager = authenticationManager;
  }

  public Authentication getAuthentication(Jws<Claims> token) {
    return new UsernamePasswordAuthenticationToken(
        token.getBody().getSubject(),
        "PROTECTED",
        AuthorityUtils.commaSeparatedStringToAuthorityList(
            token.getBody().get("roles", String.class)));
  }

  public Authentication authenticate(AuthenticationRequest authenticationRequest) {
    UsernamePasswordAuthenticationToken usernameAuthentication =
        new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword());
    return authenticationManager.authenticate(usernameAuthentication);
  }
}
