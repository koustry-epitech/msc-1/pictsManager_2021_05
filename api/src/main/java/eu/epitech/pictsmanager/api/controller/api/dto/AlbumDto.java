package eu.epitech.pictsmanager.api.controller.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 02/06/2020
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class AlbumDto {

    private String id;

    @NotNull(message = "metadata cannot be null")
    private MetadataDto metadata;

    private List<String> photoIdsList;

}
