package eu.epitech.pictsmanager.api.service;

import eu.epitech.pictsmanager.api.exception.IllegalResourceAccessException;
import eu.epitech.pictsmanager.api.exception.ResourceNotFoundException;
import eu.epitech.pictsmanager.api.model.Access;
import eu.epitech.pictsmanager.api.model.type.AccessStatusType;
import eu.epitech.pictsmanager.api.model.type.ResourceType;

import java.util.List;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 06 /06/2020
 */
public interface AccessService {

  /**
   * Load access by user list.
   *
   * @param userId the user id
   * @return the list
   */
  List<Access> loadAccessByUser(String userId);

  /**
   * Load access by resource list.
   *
   * @param resourceId the resource id
   * @param userId the user id
   * @return the list
   */
  List<Access> loadAccessByResource(String resourceId, String userId);

  /**
   * Create access access.
   *
   * @param userId the user id
   * @param resourceId the resource id
   * @param resourceType the resource type
   * @param accessType the access type
   * @return the access
   */
  Access createAccess(
      String userId, String resourceId, ResourceType resourceType, AccessStatusType accessType);

  /**
   * Delete all access by resource.
   *
   * @param resourceId the resource id
   */
  void deleteAllAccessByResource(String resourceId);

  /**
   * Can read boolean.
   *
   * @param userId the user id
   * @param resourceId the resource id
   * @return the boolean
   */
  boolean canRead(String userId, String resourceId);

  /**
   * Can update boolean.
   *
   * @param userId the user id
   * @param resourceId the resource id
   * @return the boolean
   */
  boolean canUpdate(String userId, String resourceId);

  /**
   * Can delete boolean.
   *
   * @param userId the user id
   * @param resourceId the resource id
   * @return the boolean
   */
  boolean canDelete(String userId, String resourceId);

  /**
   * Is owner boolean.
   *
   * @param userId the user id
   * @param resourceId the resource id
   * @return the boolean
   */
  boolean isOwner(String userId, String resourceId);

  /**
   * Is owner boolean.
   *
   * @param userId the user id
   * @param resourceIdList the resource id list
   * @return the boolean
   */
  boolean isOwner(String userId, List<String> resourceIdList);

  /**
   * Create readers access to album.
   *
   * @param albumId the album id
   * @param readersList the readers list
   * @param ownerId the owner id
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  void createReadersAccessToAlbum(String albumId, List<String> readersList, String ownerId)
      throws ResourceNotFoundException, IllegalResourceAccessException;

  /**
   * Delete readers access from album.
   *
   * @param albumId the album id
   * @param readersList the readers list
   * @param ownerId the owner id
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  void deleteReadersAccessFromAlbum(String albumId, List<String> readersList, String ownerId)
      throws ResourceNotFoundException, IllegalResourceAccessException;

  /**
   * Create readers access to photo.
   *
   * @param photoId the photo id
   * @param readersList the readers list
   * @param ownerId the owner id
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  void createReadersAccessToPhoto(String photoId, List<String> readersList, String ownerId)
      throws ResourceNotFoundException, IllegalResourceAccessException;

  /**
   * Delete readers access from photo.
   *
   * @param photoId the photo id
   * @param readersList the readers list
   * @param ownerId the owner id
   * @throws ResourceNotFoundException the resource not found exception
   * @throws IllegalResourceAccessException the illegal resource access exception
   */
  void deleteReadersAccessFromPhoto(String photoId, List<String> readersList, String ownerId)
      throws ResourceNotFoundException, IllegalResourceAccessException;
}
