package eu.epitech.pictsmanager.api.model;

/** The enum Role. */
public enum Role {
  /** Role admin role. */
  ROLE_ADMIN,
  /** Role user role. */
  ROLE_USER
}
