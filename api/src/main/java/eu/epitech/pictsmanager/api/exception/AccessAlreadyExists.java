package eu.epitech.pictsmanager.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * MIT License Project name : api
 *
 * @author kevinoustry
 * @since 07/07/2020
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Access already exists")
public class AccessAlreadyExists extends RuntimeException {
  public AccessAlreadyExists(String message) {
    super(message);
  }
}
