package eu.epitech.pictsmanager.api.repository;

import eu.epitech.pictsmanager.api.model.Access;
import org.springframework.data.mongodb.repository.MongoRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 06 /06/2020
 */
public interface AccessRepository extends MongoRepository<Access, String> {

  /**
   * Find by user id and resource id access.
   *
   * @param userId the user id
   * @param resourceId the resource id
   * @return the access
   */
  Access findByUserIdAndResourceId(@NotNull String userId, @NotNull String resourceId);

  /**
   * Find all by user id list.
   *
   * @param userId the user id
   * @return the list
   */
  List<Access> findAllByUserId(@NotNull String userId);

  /**
   * Find all by resource id list.
   *
   * @param resourceId the resource id
   * @return the list
   */
  List<Access> findAllByResourceId(@NotNull String resourceId);

  /**
   * Exists by user id and resource id boolean.
   *
   * @param userId the user id
   * @param resourceId the resource id
   * @return the boolean
   */
  boolean existsByUserIdAndResourceId(@NotNull String userId, @NotNull String resourceId);

  /**
   * Delete by user id and resource id.
   *
   * @param userId the user id
   * @param resourceId the resource id
   */
  void deleteByUserIdAndResourceId(@NotNull String userId, @NotNull String resourceId);
}
