package eu.epitech.pictsmanager.api.controller;

import eu.epitech.pictsmanager.api.controller.api.dto.AlbumDto;
import eu.epitech.pictsmanager.api.controller.api.dto.MetadataDto;
import eu.epitech.pictsmanager.api.model.Album;
import eu.epitech.pictsmanager.api.service.AlbumService;
import eu.epitech.pictsmanager.api.utils.AlbumMapperUtils;
import eu.epitech.pictsmanager.api.utils.MetadataMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 02 /06/2020
 */
@Slf4j
@RestController
@RequestMapping("albums")
public class AlbumController {

  private final AlbumService albumService;

  /**
   * Instantiates a new Album controller.
   *
   * @param albumService the album service
   */
  @Autowired
  public AlbumController(AlbumService albumService) {
    this.albumService = albumService;
  }

  /**
   * Gets all albums.
   *
   * @param principal the principal
   * @return the all albums
   */
  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<AlbumDto>> getAllAlbums(@AuthenticationPrincipal Principal principal) {
    log.info("Finding all albums for user {}", principal.getName());
    List<Album> albums = albumService.getAllAlbums(principal.getName());
    List<AlbumDto> dtoList =
        albums.stream().map(AlbumMapperUtils::convertToDto).collect(Collectors.toList());
    return new ResponseEntity<>(dtoList, HttpStatus.OK);
  }

  /**
   * Gets album by id.
   *
   * @param albumId the album id
   * @param principal the principal
   * @return the album by id
   */
  @GetMapping(value = "{albumId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AlbumDto> getAlbumById(
      @PathVariable String albumId, @AuthenticationPrincipal Principal principal) {
    log.info("Finding album {} for user {}", albumId, principal.getName());
    Album album = albumService.getAlbumById(albumId, principal.getName());
    AlbumDto dto = AlbumMapperUtils.convertToDto(album);
    return new ResponseEntity<>(dto, HttpStatus.OK);
  }

  /**
   * Create album response entity.
   *
   * @param albumDto the album dto
   * @param principal the principal
   * @return the response entity
   */
  @PostMapping(
      value = "create",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AlbumDto> createAlbum(
      @RequestBody AlbumDto albumDto, @AuthenticationPrincipal Principal principal) {
    log.info("Creating album {} by user {}", albumDto, principal.getName());
    Album album =
        albumService.createAlbum(AlbumMapperUtils.convertToEntity(albumDto), principal.getName());
    AlbumDto dto = AlbumMapperUtils.convertToDto(album);
    return new ResponseEntity<>(dto, HttpStatus.CREATED);
  }

  /**
   * Update album metadata response entity.
   *
   * @param albumId the album id
   * @param metadataDto the metadata dto
   * @param principal the principal
   * @return the response entity
   */
  @PutMapping("{albumId}/metadata")
  public ResponseEntity<AlbumDto> updateAlbumMetadata(
      @PathVariable String albumId,
      @RequestBody MetadataDto metadataDto,
      @AuthenticationPrincipal Principal principal) {
    log.info("Updating album metadata {} by user {}", metadataDto, principal.getName());
    Album album =
        albumService.updateAlbumMetadata(
            albumId, MetadataMapperUtils.convertToEntity(metadataDto), principal.getName());
    AlbumDto dto = AlbumMapperUtils.convertToDto(album);

    return new ResponseEntity<>(dto, HttpStatus.OK);
  }

  /**
   * Add photos to album response entity.
   *
   * @param albumId the album id
   * @param photoIds the photo ids
   * @param principal the principal
   * @return the response entity
   */
  @PostMapping(
      value = "{albumId}/photos",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AlbumDto> addPhotosToAlbum(
      @PathVariable String albumId,
      @RequestBody List<String> photoIds,
      @AuthenticationPrincipal Principal principal) {
    log.info("Adding photos {} to album {} by user {}", photoIds, albumId, principal.getName());
    Album album = albumService.addPhotosToAlbum(albumId, photoIds, principal.getName());
    return ResponseEntity.ok(AlbumMapperUtils.convertToDto(album));
  }

  /**
   * Remove photos from album response entity.
   *
   * @param albumId the album id
   * @param photoIdsList the photo ids list
   * @param principal the principal
   * @return the response entity
   */
  @DeleteMapping(
      value = "{albumId}/photos",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AlbumDto> removePhotosFromAlbum(
      @PathVariable String albumId,
      @RequestBody List<String> photoIdsList,
      @AuthenticationPrincipal Principal principal) {
    log.info(
        "Removing photos {} from album {} by user {}", photoIdsList, albumId, principal.getName());
    Album entity = albumService.removePhotosFromAlbum(albumId, photoIdsList, principal.getName());
    return ResponseEntity.ok(AlbumMapperUtils.convertToDto(entity));
  }

  /**
   * Delete album response entity.
   *
   * @param albumId the album id
   * @param principal the principal
   * @return the response entity
   */
  @DeleteMapping(value = "{albumId}")
  public ResponseEntity<Void> deleteAlbum(
      @PathVariable String albumId, @AuthenticationPrincipal Principal principal) {
    log.info("Deleting album {} by user {}", albumId, principal.getName());
    albumService.deleteAlbum(albumId, principal.getName());
    return ResponseEntity.ok(null);
  }
}
