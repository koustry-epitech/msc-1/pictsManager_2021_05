package eu.epitech.pictsmanager.api.service;

import eu.epitech.pictsmanager.api.controller.api.dto.UserDto;
import eu.epitech.pictsmanager.api.model.User;

import java.util.List;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 30 /05/2020
 */
public interface UserService {

  /**
   * Gets all users.
   *
   * @return the all users
   */
  List<User> getAllUsers();

  /**
   * Gets user by id.
   *
   * @param id the id
   * @return the user by id
   */
  User getUserById(String id);

  /**
   * Create user from dto user.
   *
   * @param userDTO the user dto
   * @return the user
   */
  User createUserFromDTO(UserDto userDTO);

  /**
   * Update user from dto user.
   *
   * @param id the id
   * @param userDto the user dto
   * @return the user
   */
  User updateUserFromDto(final String id, final UserDto userDto);

  /**
   * Delete user by id.
   *
   * @param id the id
   */
  void deleteUserById(String id);

}
