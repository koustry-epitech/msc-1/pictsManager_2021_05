package eu.epitech.pictsmanager.api.controller;

import eu.epitech.pictsmanager.api.controller.api.dto.request.AuthenticationRequest;
import eu.epitech.pictsmanager.api.controller.api.dto.response.AuthenticationResponse;
import eu.epitech.pictsmanager.api.security.AuthenticationProvider;
import eu.epitech.pictsmanager.api.security.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 15 /06/2020
 */
@Slf4j
@RestController
@RequestMapping(value = "auth")
public class AuthenticationController {

  private AuthenticationProvider authenticationProvider;

  private JwtTokenProvider jwtTokenProvider;

  /**
   * Instantiates a new Authentication controller.
   *
   * @param authenticationProvider the authentication service
   * @param jwtTokenProvider the jwt token service
   */
  @Autowired
  public AuthenticationController(
      AuthenticationProvider authenticationProvider, JwtTokenProvider jwtTokenProvider) {
    this.authenticationProvider = authenticationProvider;
    this.jwtTokenProvider = jwtTokenProvider;
  }

  /**
   * Login response entity.
   *
   * @param authenticationRequest the login dto
   * @return the response entity
   */
  @PostMapping(value = "login", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> login(@RequestBody AuthenticationRequest authenticationRequest) {
    log.info("Trying to login user {}", authenticationRequest.getUsername());
    Authentication authentication = authenticationProvider.authenticate(authenticationRequest);
    log.info("Is authenticated {}", authentication.isAuthenticated());

    if (Objects.nonNull(authentication) && authentication.isAuthenticated()) {
      String token = jwtTokenProvider.createToken(authentication);
      return ResponseEntity.ok().body(new AuthenticationResponse(token));
    }
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Authentication failed");
  }
}
