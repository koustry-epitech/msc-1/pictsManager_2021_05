package eu.epitech.pictsmanager.api.repository;

import eu.epitech.pictsmanager.api.model.AbstractBaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * MIT License
 * Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 02/06/2020
 */
public interface BaseModelRepository extends MongoRepository<AbstractBaseModel, String> {
}
