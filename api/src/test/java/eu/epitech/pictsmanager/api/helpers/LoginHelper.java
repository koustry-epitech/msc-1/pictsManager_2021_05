package eu.epitech.pictsmanager.api.helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.epitech.pictsmanager.api.controller.api.dto.request.AuthenticationRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class LoginHelper {

  private String username;
  private String password;
  private String headerValue;
  private MockMvc mvc;

  ObjectMapper mapper = new ObjectMapper();

  public LoginHelper(MockMvc mvc) {
    this.mvc = mvc;
  }

  public LoginHelper username(String username) {
    this.username = username;
    return this;
  }

  public LoginHelper password(String password) {
    this.password = password;
    return this;
  }

  public LoginHelper login() {
    AuthenticationRequest authDto = new AuthenticationRequest();
    authDto.setUsername("admin");
    authDto.setPassword("admin");
    try {
      MvcResult loginMvcResult = mvc.perform(MockMvcRequestBuilders.post("/auth/login")
        .content(this.mapper.writeValueAsBytes(authDto))
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .accept(MediaType.ALL))
        .andReturn();

      JsonNode resTree = mapper.readTree(loginMvcResult.getResponse().getContentAsString());

      StringBuilder headerContent = new StringBuilder();
      headerContent.append(resTree.get("authenticationType").asText() + " ");
      headerContent.append(resTree.get("accessToken").asText());
      this.headerValue = headerContent.toString();
    } catch (Exception e) {
      throw new RuntimeException("Error when login", e);
    }
    return this;
  }

  public String getHeaderName() {
    return "Authorization";
  }

  public String getHeaderValue() {
    return this.headerValue;
  }

}
