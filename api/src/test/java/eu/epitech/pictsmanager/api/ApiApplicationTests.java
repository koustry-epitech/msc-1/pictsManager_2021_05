package eu.epitech.pictsmanager.api;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */

/** The type Api application tests. */
@SpringBootTest
class ApiApplicationTests {

  /** Context loads. */
  @Test
  void contextLoads() {}
}
