package eu.epitech.pictsmanager.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.epitech.pictsmanager.api.controller.PhotoController;
import eu.epitech.pictsmanager.api.controller.SearchController;
import eu.epitech.pictsmanager.api.controller.api.dto.AlbumDto;
import eu.epitech.pictsmanager.api.controller.api.dto.MetadataDto;
import eu.epitech.pictsmanager.api.controller.api.dto.SearchResultDto;
import eu.epitech.pictsmanager.api.helpers.LoginHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author Lucas Fabre
 * @since 16/06/2020
 */

/** The type Api application tests. */
@SpringBootTest
@AutoConfigureMockMvc
@EnableWebMvc
class SearchApplicationTests {

  private static final ObjectMapper mapper = new ObjectMapper();
  @Value("classpath:testpic.jpg")
  Resource testPicture;
  @Autowired private SearchController searchController;
  @Autowired private PhotoController photoController;
  @Autowired private MockMvc mvc;

  /** Context loads */
  @Test
  void contextLoads() {}

  @Test
  public void testSearchPhoto() throws Exception {
    byte[] pictureAsBytes = testPicture.getInputStream().readAllBytes();

    LoginHelper login = new LoginHelper(this.mvc).username("admin").password("admin").login();

    MvcResult postPictureDataMvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/photos/data")
                    .content(pictureAsBytes)
                    .contentType(MediaType.IMAGE_JPEG)
                    .header(login.getHeaderName(), login.getHeaderValue())
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();

    String pictureId =
        mapper
            .readTree(postPictureDataMvcResult.getResponse().getContentAsString())
            .get("id")
            .asText();

    MetadataDto metadata = new MetadataDto();
    metadata.setTitle("I am a test picture");
    metadata.setDescription("I was created by the test");
    List<String> tags = new ArrayList<String>();
    tags.add("elem:dog");
    metadata.setTags(tags);

    mvc.perform(
            MockMvcRequestBuilders.post(
                    "/photos/"
                        + pictureId
                        + "/metadata") // We do not need the result for this request
                .header(login.getHeaderName(), login.getHeaderValue())
                .content(mapper.writeValueAsString(metadata))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is(201));

    MvcResult searchMvceResult =
        mvc.perform(
                MockMvcRequestBuilders.get("/search/dog")
                    .header(login.getHeaderName(), login.getHeaderValue())
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();

    SearchResultDto searchResult =
        mapper.readValue(
            searchMvceResult.getResponse().getContentAsString(), SearchResultDto.class);
    boolean containsPicure =
        searchResult.getPhotos().stream()
            .filter(p -> pictureId.equals(p.getId()))
            .findAny()
            .isPresent();

    assertTrue(containsPicure, "the search result does not contains the picture");
  }

  @Test
  public void testSearchNotFound() throws Exception {
    byte[] pictureAsBytes = testPicture.getInputStream().readAllBytes();

    LoginHelper login = new LoginHelper(this.mvc).username("admin").password("admin").login();

    MvcResult postPictureDataMvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/photos/data")
                    .content(pictureAsBytes)
                    .contentType(MediaType.IMAGE_JPEG)
                    .header(login.getHeaderName(), login.getHeaderValue())
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();

    String pictureId =
        mapper
            .readTree(postPictureDataMvcResult.getResponse().getContentAsString())
            .get("id")
            .asText();

    AlbumDto album = new AlbumDto();

    MvcResult createAlbumMvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/photos/data")
                    .content(pictureAsBytes)
                    .contentType(MediaType.IMAGE_JPEG)
                    .header(login.getHeaderName(), login.getHeaderValue())
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();

    MetadataDto metadata = new MetadataDto();
    metadata.setTitle("I am a test picture");
    metadata.setDescription("I was created by the test");
    List<String> tags = new ArrayList<String>();
    tags.add("elem:dog");
    metadata.setTags(tags);

    mvc.perform(
            MockMvcRequestBuilders.post(
                    "/photos/"
                        + pictureId
                        + "/metadata") // We do not need the result for this request
                .header(login.getHeaderName(), login.getHeaderValue())
                .content(mapper.writeValueAsString(metadata))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is(201));

    MvcResult searchMvceResult =
        mvc.perform(
                MockMvcRequestBuilders.get("/search/cat")
                    .header(login.getHeaderName(), login.getHeaderValue())
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();

    SearchResultDto searchResult =
        mapper.readValue(
            searchMvceResult.getResponse().getContentAsString(), SearchResultDto.class);
    boolean containsPicure =
        searchResult.getPhotos().stream()
            .filter(p -> pictureId.equals(p.getId()))
            .findAny()
            .isPresent();

    assertFalse(containsPicure, "the search result contains the picture");
  }
}
