package eu.epitech.pictsmanager.api.utils;

import eu.epitech.pictsmanager.api.controller.api.dto.UserDto;
import eu.epitech.pictsmanager.api.model.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * MIT License Project name : pictsManager_2019_05
 *
 * @author kevinoustry
 * @since 01 /06/2020
 */
public class UserMapperUtilsTest {

  /** Convert user entity to user dto. */
  @Test
  public void convertUserEntityToUserDto() {
    UserDto userDto;
    User user = new User();

    user.setId("5ed4d565b29dfe00afd3ec27");
    user.setUsername("Jean Claude");
    user.setPassword("pass");
    userDto = UserMapperUtils.convertToDto(user);

    assertEquals(user.getId(), userDto.getId());
    assertEquals(user.getUsername(), userDto.getUsername());
    assertEquals(user.getPassword(), userDto.getPassword());
  }

  /** Convert user dto to user entity. */
  @Test
  public void convertUserDtoToUserEntity() {
    User entity;
    UserDto dto = new UserDto();

    dto.setId("5ed4d565b29dfe00afd3ec27").setUsername("Joe").setPassword("abc");
    entity = UserMapperUtils.convertToEntity(dto);

    assertEquals(dto.getId(), entity.getId());
    assertEquals(dto.getUsername(), entity.getUsername());
    assertEquals(dto.getPassword(), entity.getPassword());
  }
}
