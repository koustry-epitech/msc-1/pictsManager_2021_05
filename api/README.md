# API 

Package for the backend api using [Java 11](https://docs.oracle.com/en/java/javase/11/) with [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/html/).

## 🚀 Getting started 

> Installation

``` JSON 
$ ./gradlew build 
```

> Running 

``` JSON 
$ java -jar build/libs/api-0.0.1-SNAPSHOT.jar
```

> Testing 

1. Run unit tests:

``` JSON 
$ ./gradlew build test
```

> Generate documentation

``` JSON 
$ javadoc -d ./javadoc src\*
```

2. Run Postman test:

## 🏗 Project architecture 

``` JS
Package name: eu.epitech.pictsmanager.api
├── config                  // application config 
├── controller              
│   └── api                 // api resources controllers
│       └── dto             // api dto 
│           ├── request     // api dto request 
│           └── response    // api dto response 
├── dto                     // general dto 
├── exception               // custom exceptions 
├── init                    // init script 
├── model                   // domains models 
│   └── type                // custom types 
├── repository              // dao layer for mongoDB 
├── security                // spring security 
├── service                 // business logic services 
│   └── impl                // interfaces implementation 
└── utils                   // utility classes 
```

## 🍻 API documentation  

We use OpenAPI 2 and Swagger for documenting the API, check the following routes:
- /api/swagger-ui.html (Swagger UI)
- /api/v2/api-docs (OpenAPI JSON)
