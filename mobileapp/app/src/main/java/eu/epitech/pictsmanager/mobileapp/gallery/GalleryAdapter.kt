package eu.epitech.pictsmanager.mobileapp.gallery

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.epitech.pictsmanager.mobileapp.ImageGridKotlinAdapter
import eu.epitech.pictsmanager.mobileapp.R
import kotlinx.android.synthetic.main.list_item_photo.view.*
import java.sql.Date
import java.time.LocalTime
import java.util.*


class GalleryAdapter (private val context: Context,
                      private val dataSource: List<List<Photo>>) : RecyclerView.Adapter<GalleryAdapter.ListViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(
            LayoutInflater.from(context).inflate(R.layout.list_item_photo, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        //holder.tvDate.text = dataSource[position].first().dateTime.toString()
        holder.tvDate.text = LocalTime.now().toString()
        holder.rv.layoutManager = GridLayoutManager(context, 3)
        holder.rv.adapter =
            ImageGridKotlinAdapter(
                context,
                dataSource[position]
            )
    }

    class ListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvDate = view.tvDate as TextView
        val rv = view.rvPhotos as RecyclerView
    }
}

