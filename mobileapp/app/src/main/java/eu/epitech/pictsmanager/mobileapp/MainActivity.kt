package eu.epitech.pictsmanager.mobileapp

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import eu.epitech.pictsmanager.mobileapp.album.AlbumFragment
import eu.epitech.pictsmanager.mobileapp.album.AlbumViewModel
import eu.epitech.pictsmanager.mobileapp.camera.CameraFragment
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryFragment
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryViewModel
import eu.epitech.pictsmanager.mobileapp.home.LoginFragment
import eu.epitech.pictsmanager.mobileapp.home.UserViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var userViewModel: UserViewModel
    private lateinit var albumViewModel: AlbumViewModel

    private val SERVER_URL = "http://51.38.188.254:8080/"

    override fun onCreate(savedInstanceState: Bundle?) {
        //Link AppPreference to the Activity Context to use SharedPreference as Global Variable storage
        AppPreferences.setup(applicationContext)

        //Set the global Variable SERVER_URL for retrofit call
        AppPreferences.setURL(SERVER_URL)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        //------ Condition hide login form when user is auth
        val fragment = LoginFragment()
        pushFragment(fragment, this)
        //------ End condition

    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.bottomNavigationGalleryMenuId -> {
                    val fragment = GalleryFragment()
                    pushFragment(fragment, this)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.bottomNavigationCameraMenuId -> {
                    val fragment = CameraFragment()
                    pushFragment(fragment,this)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.bottomNavigationAlbumMenuId -> {
                    val fragment = AlbumFragment()
                    pushFragment(fragment, this)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    /** Check if this device has a camera */
    private fun checkCameraHardware(context: Context): Boolean {
        return context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)
    }

    private fun pushFragment(newFragment: Fragment, context: Context) {
        val transaction: FragmentTransaction =
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, newFragment)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}
