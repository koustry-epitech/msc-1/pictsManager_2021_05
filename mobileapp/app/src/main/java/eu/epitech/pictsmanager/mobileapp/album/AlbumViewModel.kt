package eu.epitech.pictsmanager.mobileapp.album

import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import eu.epitech.pictsmanager.mobileapp.Metadata
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import eu.epitech.pictsmanager.mobileapp.gallery.Photo
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AlbumViewModel: ViewModel() {
    private val _albums = MutableLiveData<MutableList<Album>>()
    val albums: LiveData<MutableList<Album>> get() = _albums

    private val _albumsDisplay = MutableLiveData<MutableList<AlbumDisplay>>()
    val albumsDisplay: LiveData<MutableList<AlbumDisplay>> get() = _albumsDisplay

    fun addAlbum(album: Album) {
        if(albums.value.isNullOrEmpty())
        {
            val list : MutableList<Album> = mutableListOf()
            list.add(album)
            _albums.value = list

        }
        else {
            val index = _albums.value!!.binarySearch { String.CASE_INSENSITIVE_ORDER.compare(it.id, album.id) }
            if(index == -1)
                albums.value!!.add(album)
            else
            {
                val photoSearch = albums.value?.find{ it.id == album.id}
                _albums.value?.remove(photoSearch)
                albums.value!!.add(album)
            }
        }
    }

    fun addAlbumDisplay(albumDisplay: AlbumDisplay) {
        if(albumsDisplay.value.isNullOrEmpty())
        {
            val list : MutableList<AlbumDisplay> = mutableListOf()
            list.add(albumDisplay)
            _albumsDisplay.value = list

        }
        else {
            val index = _albumsDisplay.value!!.binarySearch { String.CASE_INSENSITIVE_ORDER.compare(it.albumID, albumDisplay.albumID) }
            if(index == -1)
                albumsDisplay.value!!.add(albumDisplay)
            else
            {
                val albumSearch = albumsDisplay.value?.find{ it.albumID == albumDisplay.albumID}
                _albumsDisplay.value?.remove(albumSearch)
                albumsDisplay.value!!.add(albumDisplay)
            }
        }
    }

    fun getFirstImage(album: Album) : String
    {
        return album.photoIdsList!![0]
    }

    fun retrieveAlbumDisplay(id: String?): AlbumDisplay? {
        if(!albums.value.isNullOrEmpty())
        {
            val index = _albumsDisplay.value!!.binarySearch { String.CASE_INSENSITIVE_ORDER.compare(it.albumID, id) }
            return albumsDisplay.value!![index]
        }
        return null
    }

}