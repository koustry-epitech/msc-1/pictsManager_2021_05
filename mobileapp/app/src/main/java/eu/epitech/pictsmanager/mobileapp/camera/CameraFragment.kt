package eu.epitech.pictsmanager.mobileapp.camera

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.Surface
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.common.util.concurrent.ListenableFuture
import eu.epitech.pictsmanager.mobileapp.Metadata
import eu.epitech.pictsmanager.mobileapp.R
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryAdapter
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryFragment
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryViewModel
import eu.epitech.pictsmanager.mobileapp.gallery.Photo
import eu.epitech.pictsmanager.mobileapp.helpers.CompressImage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.camera_fragment.*
import kotlinx.android.synthetic.main.gallery_fragment.*
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.util.concurrent.Executors
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

class CameraFragment : Fragment() {

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var imagePreview: Preview
    private lateinit var imageCapture: ImageCapture
    private lateinit var previewView: PreviewView
    private val executor = Executors.newSingleThreadExecutor()
    private lateinit var cameraControl: CameraControl
    private lateinit var cameraInfo: CameraInfo
    private var cameraSel: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.camera_fragment, container, false)
        this.activity?.bottomNavigationView?.menu?.getItem(1)?.isChecked = true //Fix for the first time
        this.activity?.bottomNavigationView?.visibility = View.VISIBLE
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        previewView = viewFinder

        if (allPermissionsGranted()) {
            previewView.post { startCamera() }
        } else {
            requestPermissions(
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }

        take_picture_button.setOnClickListener {
            takePicture()
        }
        switch_camera.setOnClickListener {
            switchCamera()
        }
    }

    private fun takePicture() {
        imageCapture.takePicture(executor, object: ImageCapture.OnImageCapturedCallback() {
            @SuppressLint("UnsafeExperimentalUsageError")
            override fun onCaptureSuccess(image: ImageProxy) {
                val buffer = image.planes[0].buffer
                val bytes = ByteArray(buffer.capacity())
                buffer.get(bytes)
                var bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                bitmap = flip(bitmap)
                runOnUiThread {
                    outputImage.visibility = View.VISIBLE
                    loading.visibility = View.VISIBLE
                    outputImage.setImageBitmap(bitmap)
                    CompressTask().execute(bitmap)
                }

                super.onCaptureSuccess(image)
            }

            override fun onError(exception: ImageCaptureException) {
                val msg = "Photo capture failed: ${exception.message}"
                previewView.post {
                    Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    @SuppressLint("RestrictedApi")
    private fun startCamera(selector: Int = cameraSel) {
        imagePreview = Preview.Builder().apply {
            setTargetResolution(Size(480,640))
            setTargetRotation(Surface.ROTATION_0)
        }.build()

        imageCapture = ImageCapture.Builder().apply {
            setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            setTargetResolution(Size(480,640))
            setTargetRotation(Surface.ROTATION_0)
        }.build()

        val cameraSelector = CameraSelector.Builder().requireLensFacing(selector).build()
        cameraProviderFuture.addListener(Runnable {
            val cameraProvider = cameraProviderFuture.get()
            cameraProvider.unbindAll()
            val camera = cameraProvider.bindToLifecycle(
                this,
                cameraSelector,
                imagePreview,
                imageCapture
            )
            previewView.preferredImplementationMode = PreviewView.ImplementationMode.TEXTURE_VIEW
            imagePreview.setSurfaceProvider(previewView.createSurfaceProvider())

            cameraControl = camera.cameraControl
            cameraInfo = camera.cameraInfo
        }, ContextCompat.getMainExecutor(requireContext()))
    }


    private fun switchCamera(){
        cameraSel = if(cameraSel == 0) 1 else 0
        startCamera(cameraSel)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                previewView.post { startCamera() }
            } else {
                activity?.finish()
            }
        }
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    private fun flip(src: Bitmap): Bitmap
    {
        // create new matrix for transformation
        val matrix = Matrix();
        matrix.setRotate((-90).toFloat())
        matrix.preScale(1.0f, -1.0f)

        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.width, src.height, matrix, false)
    }

    companion object {
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }

    @SuppressLint("StaticFieldLeak")
    inner class CompressTask: AsyncTask<Bitmap, Void, Bitmap>() {

        override fun doInBackground(vararg params: Bitmap): Bitmap {
            val result = CompressImage().calculate(params[0])
            Log.d("SIZE uncompressed" , params[0].byteCount.toString())
            Log.d("SIZE compressed" , result.byteCount.toString())
            return result
        }

        override fun onPostExecute(result: Bitmap?) {
            outputImage.setImageBitmap(result)
            loading.visibility = View.GONE
            runOnUiThread {
                val stream = ByteArrayOutputStream()
                result?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                val byteArray : ByteArray = stream.toByteArray()
                sendImage(byteArray)
                outputImage.visibility = View.GONE
            }
        }
    }

    fun Fragment?.runOnUiThread(action: () -> Unit) {
        this ?: return
        if (!isAdded) return // Fragment not attached to an Activity
        activity?.runOnUiThread(action)
    }


    fun sendImage(byteArray: ByteArray){
        val mediaType = MediaType.parse("image/jpeg")
        var body = RequestBody.create(mediaType, byteArray)

        val service = RetrofitService.retrofitService()
        val connectRequest: Call<ResponseBody> = service.postPhotoData(body)
        connectRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body().let {
                    val jsonObject = JSONObject(it!!.string())
                    val idImage = jsonObject.getString("id")
                    Log.d("BODY", jsonObject.getString("id"))

                    sendImageMetadata(idImage)
                }
            }
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }

    private fun sendImageMetadata(idImage: String){

        val df = android.text.format.DateFormat.format("dd/MM/yyyy", Date())
        val body = Metadata()

        body.title = df.toString()
        body.description = "lorem ipsum"
        body.tags = listOf("toulouse", "test", "moi")

        val service = RetrofitService.retrofitService()
        val connectRequest: Call<ResponseBody> = service.postPhotoMetadata(idImage, body)
        connectRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body().let {

                }
                runOnUiThread {
                    activity?.let {
                        val galleryViewModel = ViewModelProviders.of(it).get(GalleryViewModel::class.java)
                        val photo = Photo()
                        photo.id = idImage
                        galleryViewModel.downloadPhoto(photo)
                    }
                }

            }
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }

}