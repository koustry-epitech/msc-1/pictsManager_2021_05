package eu.epitech.pictsmanager.mobileapp

import android.media.Image
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import eu.epitech.pictsmanager.mobileapp.album.Album
import eu.epitech.pictsmanager.mobileapp.gallery.Photo
import eu.epitech.pictsmanager.mobileapp.home.Auth
import eu.epitech.pictsmanager.mobileapp.home.User
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*
import java.io.File

interface APIService {
/**************************************************************************************************
 *                              SEARCH
 ***********************************************************************************************/
    // Call back to search photos and albums
    @GET("/api/search/{search}")
    fun searchQuery(@Path("search") search: String): Call<JsonObject>



/**************************************************************************************************
 *                              PHOTOS
 ***********************************************************************************************/
    // Retrieve list of the photo's ID as string
    @GET("/api/photos/getAllIds")
    fun getPhotos(): Call<JsonArray>

    // Get a photo ID as param and retrieve the image of the photo
    @GET("/api/photos/{photoId}/data")
    fun getPhotoData(@Path("photoId") photoId: String): Call<ResponseBody>

    // Get a photo ID as param and retrieve the metadata of the photo
    @GET("/api/photos/{photoId}/metadata")
    fun getPhotoMetaData(@Path("photoId") photoId: String): Call<Metadata>

    // Send picture
    @POST("/api/photos/data")
    fun postPhotoData(@Body requestBody: RequestBody): Call<ResponseBody>

    // Send picture metadata
    @POST("/api/photos/{photoId}/metadata")
    fun postPhotoMetadata(@Path("photoId") photoId: String, @Body metadata: Metadata): Call<ResponseBody>

    @DELETE("/api/photos/{photoId}")
    fun deletePhoto(@Path("photoId") photoId: String): Call<ResponseBody>

/**************************************************************************************************
 *                              ALBUMS
 ***********************************************************************************************/
    // Retrieve list of albums
    @GET("/api/albums")
    fun getAlbums(): Call<JsonArray>

    // Retrieve a specific album
    @GET("/api/albums/{albumId}")
    fun getAlbumById(@Path("albumId") albumId: String): Call<Album>

    //Update album metadata
    @PUT("/api/albums/{albumId}/metadata")
    fun putAlbumMetadata(@Path("albumId")albumId: String, @Body metadata: Metadata): Call<ResponseBody>

    //Delete album
    @DELETE("/api/albums/{albumId}")
    fun deleteAlbum(@Path("albumId")albumId: String): Call<ResponseBody>

/**************************************************************************************************
 *                              USERS
 ***********************************************************************************************/
    //Create a user from an username and a password (register)
    @POST("/api/users")
    fun registerUser(@Body user: User) : Call<ResponseBody>


/**************************************************************************************************
 *                              AUTH
 ***********************************************************************************************/
    //Auth user
    @POST("/api/auth/login")
    fun connectUser(@Body user: User): Call<Auth>

/**************************************************************************************************
 *                              ACCESS
 ***********************************************************************************************/

    @POST("/api/access/photos/{photoId}")
    fun addUserAccessToPhoto(@Path("photoId") photoId: String, @Body array: Array<String>): Call<ResponseBody>

    @DELETE("/api/access/photos/{photoId}")
    fun deleteUserAccessToPhoto(@Path("photoId") photoId: String, @Body array: Array<String>): Call<ResponseBody>

    @POST("/api/access/albums/{albumId}")
    fun addUserAccessToAlbum(@Path("albumId") albumId: String, @Body array: Array<String>): Call<ResponseBody>

    @DELETE("/api/access/albums/{albumId}")
    fun deleteUserAccessToAlbum(@Path("albumId") albumId: String, @Body array: Array<String>): Call<ResponseBody>

}