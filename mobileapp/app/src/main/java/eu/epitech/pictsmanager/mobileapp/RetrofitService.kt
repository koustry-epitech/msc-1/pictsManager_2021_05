package eu.epitech.pictsmanager.mobileapp

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {

    fun retrofitService(): APIService {
        val interceptor = AuthInterceptor()
        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(AppPreferences.getURL())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit.create(APIService::class.java)
    }

    fun authRetrofitService(): APIService {
        val retrofit = Retrofit.Builder()
            .baseUrl(AppPreferences.getURL())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(APIService::class.java)
    }


}