package eu.epitech.pictsmanager.mobileapp.home

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.GsonBuilder
import eu.epitech.pictsmanager.mobileapp.APIService
import eu.epitech.pictsmanager.mobileapp.AppPreferences
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class UserViewModel(application: Application) : AndroidViewModel(application) {

    fun register(username: String, password: String, registerFragment: RegisterFragment)
    {
        val service = RetrofitService.authRetrofitService()

        val user = User(username, password)
        val registerRequest = service.registerUser(user)
        registerRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                registerFragment.registerAccomplished()
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }

    fun connect(
        username: String,
        password: String,
        loginFragment: LoginFragment
    )
    {
        val service = RetrofitService.authRetrofitService()

        val user = User(username, password)
        val connectRequest = service.connectUser(user)
        connectRequest.enqueue(object: Callback<Auth> {
            override fun onResponse(call: Call<Auth>, response: Response<Auth >) {
                response.body()?.let {
                    AppPreferences.setToken(it.accessToken!!)
                    loginFragment.connectAfterRequest()
                }


            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<Auth>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }
}