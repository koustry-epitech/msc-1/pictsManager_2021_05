package eu.epitech.pictsmanager.mobileapp.Search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import eu.epitech.pictsmanager.mobileapp.album.Album
import eu.epitech.pictsmanager.mobileapp.gallery.Photo
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchViewModel : ViewModel() {
    private val _photos = MutableLiveData<MutableList<Photo>>()
    val photos: LiveData<MutableList<Photo>> get() = _photos

    private val _albums = MutableLiveData<MutableList<Album>>()
    val albums: LiveData<MutableList<Album>> get() = _albums

    private val _query = MutableLiveData<String>()
    val query: LiveData<String> get() = _query

    init {
        _query.value = ""
    }

    fun refreshSearch(search: String?) {
        initViewModel()

        _query.value = search
        if(search.isNullOrEmpty())
            clearViewModel()
        else {
            val service =
                RetrofitService.retrofitService()
            val photos = service.searchQuery(search)
            photos.enqueue(object: Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    if (response.isSuccessful) {
                        try {
                            val searchResult = Gson().fromJson(response.body(), Search::class.java)
                            _albums.value = searchResult.albums
                            _photos.value = searchResult.photos
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Log.e("TAN", "Error : $t")
                }
            })
        }


    }

    private fun initViewModel() {
        if(photos.value.isNullOrEmpty())
        {
            val list : MutableList<Photo> = mutableListOf()
            _photos.value = list
        }

        if(albums.value.isNullOrEmpty())
        {
            val list : MutableList<Album> = mutableListOf()
            _albums.value = list
        }
    }

    private fun clearViewModel() {
        _photos.value!!.clear()
        _albums.value!!.clear()
    }
}