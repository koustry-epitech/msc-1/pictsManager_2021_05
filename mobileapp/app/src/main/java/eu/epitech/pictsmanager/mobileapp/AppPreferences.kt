package eu.epitech.pictsmanager.mobileapp

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

object AppPreferences {
    private var sharedPreferences: SharedPreferences? = null

    fun setup(context: Context) {
        sharedPreferences = context.getSharedPreferences("PICT_MANAGER.sharedprefs", MODE_PRIVATE)
    }

    fun setToken(content: String) {
        sharedPreferences!!.edit().putString("TOKEN",content).apply()
    }

    fun getToken(): String {
        return sharedPreferences!!.getString("TOKEN",null) ?: ""
    }

    fun setURL(content: String) {
        sharedPreferences!!.edit().putString("SERVER_URL",content).apply()
    }

    fun getURL(): String {
        return sharedPreferences!!.getString("SERVER_URL",null) ?: ""
    }

    private enum class Key {
        TOKEN;

        fun getString(): String? = if (sharedPreferences!!.contains(name)) sharedPreferences!!.getString(name, "") else null
        fun setString(value: String?) = value?.let { sharedPreferences!!.edit().putString(name, value) }

        fun remove() = sharedPreferences!!.edit().remove(name)
    }
}