package eu.epitech.pictsmanager.mobileapp.helpers

import android.graphics.Bitmap
import android.util.Log
import java.util.*
import kotlin.math.abs


class CompressImage(){

    private lateinit var clusters: Array<Cluster?>
var i = 0

    init {
        Log.d("CompressImage", "Compression Start")
    }

    fun calculate(image: Bitmap, k: Int = 32): Bitmap {
        Log.d("CompressImage", "Calculation ...")
        val start = System.currentTimeMillis()
//        val bitmap = transformBitmap(image)
        val bitmap = image
        val w: Int = bitmap.width
        val h: Int = bitmap.height
        // Creation des clusters
        clusters = createClusters(bitmap, k)
        // Creation d'une table de recherche de cluster : index -1 par defaut
        val lut = IntArray(w * h)
        var pixelChangedCluster = true
        Arrays.fill(lut, -1)
        // On boucle sur tous les pixels pour les mettre dans leur cluster
        // Tant qu'il y a des clusters
        var loops = 0
        while (pixelChangedCluster) {
            pixelChangedCluster = false
            loops++
            Log.d("CompressImage", "Loop n°$loops")
            for (y in 0 until h) {
                for (x in 0 until w) {
                    var pixel: Int = bitmap.getPixel(x, y)
                    val cluster = findMinimalCluster(pixel)
                    if (lut[w * y + x] != cluster!!.id) { // cluster changed
                        if (lut[w * y + x] != -1) { // remove from possible previous
                            clusters[lut[w * y + x]]!!.removePixel(pixel)
                        }
                        // Ajout du pixel au cluster
                        cluster.addPixel(pixel)
                        // On continue de boucler
                        pixelChangedCluster = true
                        // On update la table de recherche
                        lut[w * y + x] = cluster.id
                    }
                }
            }
        }
        // Creation de la nouvelle image
        Log.d("CompressImage", "Recreate Image ...")
        val result = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565)
        for (y in 0 until h) {
            for (x in 0 until w) {
                val clusterId = lut[w * y + x]
                result.setPixel(x, y, clusters[clusterId]!!.rGB)
            }
        }
        val end = System.currentTimeMillis()
        Log.d("CompressImage", "$k clusters in $loops loops in " + (end - start) + " ms.")
        return result
    }

    private fun createClusters(image: Bitmap, k: Int): Array<Cluster?> {
        val result = arrayOfNulls<Cluster>(k)
        var x = 0
        var y = 0
        val dx: Int = image.width / k
        val dy: Int = image.height / k
        for (i in 0 until k) {
            result[i] = Cluster(i, image.getPixel(x, y))
            x += dx
            y += dy
        }
        return result
    }

    private fun findMinimalCluster(rgb: Int): Cluster? {
        var cluster: Cluster? = null
        var min = Int.MAX_VALUE
        for (i in clusters.indices) {
            val distance = clusters[i]!!.distance(rgb)
            if (distance < min) {
                min = distance
                cluster = clusters[i]
            }
        }
        return cluster
    }

    internal inner class Cluster(id: Int, rgb: Int) {
        var id: Int
        var pixelCount = 0
        var red: Int
        var green: Int
        var blue: Int
        var reds = 0
        var greens = 0
        var blues = 0
        var redMask = 0x000000FF
        var greenMask = 0x000000FF
        var blueMask = 0x000000FF

        val rGB: Int
            get() {
                val r = reds / pixelCount
                val g = greens / pixelCount
                val b = blues / pixelCount
                return -0x1000000 or (r shl 16) or (g shl 8) or b
            }

        fun addPixel(color: Int) {
            val r = color shr 16 and redMask
            val g = color shr 8 and greenMask
            val b = color and blueMask
            reds += r
            greens += g
            blues += b
            pixelCount++
            red = reds / pixelCount
            green = greens / pixelCount
            blue = blues / pixelCount
        }

        fun removePixel(color: Int) {
            val r = color shr 16 and redMask
            val g = color shr 8 and greenMask
            val b = color and blueMask
            reds -= r
            greens -= g
            blues -= b
            pixelCount--
            red = reds / pixelCount
            green = greens / pixelCount
            blue = blues / pixelCount
        }

        fun distance(color: Int): Int {
            val r = color shr 16 and redMask
            val g = color shr 8 and greenMask
            val b = color and blueMask
            val rx = abs(red - r)
            val gx = abs(green - g)
            val bx = abs(blue - b)
            return (rx + gx + bx) / 3
        }

        init {
            val r = rgb shr 16 and redMask
            val g = rgb shr 8 and greenMask
            val b = rgb and blueMask
            red = r
            green = g
            blue = b
            this.id = id
            addPixel(rgb)
        }
    }
}