package eu.epitech.pictsmanager.mobileapp.album

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import eu.epitech.pictsmanager.mobileapp.R
import eu.epitech.pictsmanager.mobileapp.single.SingleAlbumFragment
import kotlinx.android.synthetic.main.album_element.view.*

class AlbumAdapter(private val c: Context, private val albumsDisplay: MutableList<AlbumDisplay>, private val albums: MutableList<Album>) :
    RecyclerView.Adapter<AlbumAdapter.ColorViewHolder>() {


    override fun getItemCount(): Int {
        return albumsDisplay.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        return ColorViewHolder(
            LayoutInflater.from(c).inflate(R.layout.album_element, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {

        holder.iv.setImageBitmap(albumsDisplay[position].photo)
        holder.tv.text = albumsDisplay[position].title

        holder.iv.setOnClickListener {
            //handle click event on image
            pushFragment(SingleAlbumFragment(albums[position]), c)
        }
    }

    private fun pushFragment(newFragment: Fragment, context: Context) {
        val transaction: FragmentTransaction =
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, newFragment)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    class ColorViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val iv = view.ivAlbum as ImageView
        val tv = view.tvTitleAlbum as TextView
    }
}