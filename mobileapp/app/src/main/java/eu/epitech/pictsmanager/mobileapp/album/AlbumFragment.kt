package eu.epitech.pictsmanager.mobileapp.album

import android.graphics.Bitmap
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.google.gson.JsonArray
import eu.epitech.pictsmanager.mobileapp.R
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import eu.epitech.pictsmanager.mobileapp.Search.SearchViewModel
import eu.epitech.pictsmanager.mobileapp.databinding.AlbumFragmentBinding
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlinx.android.synthetic.main.search_bar.searchText

class AlbumFragment : Fragment() {

    private lateinit var albumViewModel: AlbumViewModel
    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var searchViewModel: SearchViewModel
    private lateinit var binding: AlbumFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.album_fragment, container, false)

        activity?.let {
            albumViewModel = ViewModelProviders.of(it).get(AlbumViewModel::class.java)
            galleryViewModel = ViewModelProviders.of(it).get(GalleryViewModel::class.java)
            searchViewModel = ViewModelProviders.of(it).get(SearchViewModel::class.java)
            albumViewModel.albums.observe(this, Observer { updateList() })
            searchViewModel.albums.observe(this, Observer { updateSearch() })
        }

        binding.rvAlbums.layoutManager = GridLayoutManager(context, 2)
        val searchBar = binding.searchBar.searchText
        searchBar.setText(searchViewModel.query.value)
        searchBar.addTextChangedListener(object : TextWatcher {
            /**
             * This method is called to notify you that, somewhere within
             * `s`, the text has been changed.
             */
            override fun afterTextChanged(s: Editable?) {
                searchViewModel.refreshSearch(searchBar.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        val service = RetrofitService.retrofitService()

        val photos = service.getAlbums()
        photos.enqueue(object: Callback<JsonArray> {
            override fun onResponse(call: Call<JsonArray>, response: Response<JsonArray>) {
                response.body()?.let {
                    runOnUiThread {

                        for(i in 0 until it.size())
                        {
                            val elem = it[i]
                            Log.i("elem", elem.toString())
                            val album: Album = Gson().fromJson(elem, Album::class.java)
                            albumViewModel.addAlbum(album)
                            val photoID = albumViewModel.getFirstImage(album)
                            galleryViewModel.getPhoto(album, photoID, this@AlbumFragment)
                            // updateList() Normalement update automatiquement
                        }
                    }
                }
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<JsonArray>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })

        return binding.root
    }

    private fun updateList() {
        Log.e("Update", "Update success Album")
        //val photoString : MutableList<String> = albumViewModel.getFirstImages()

        albumViewModel.albumsDisplay.value?.let{
            binding.rvAlbums.adapter = AlbumAdapter(context!!, it, albumViewModel.albums.value!!)
        }
    }

    private fun updateSearch() {
        if(!searchViewModel.query.value.isNullOrEmpty()){
            searchViewModel.albums.value?.let{
                val listAlbums = mutableListOf<AlbumDisplay>()
                for(album in it)
                {
                    listAlbums.add(albumViewModel.retrieveAlbumDisplay(album.id)!!)
                }
                binding.rvAlbums.adapter = AlbumAdapter(context!!, listAlbums, searchViewModel.albums.value!!)
            }
        }
        else
        updateList()
    }

    fun addAlbumImage(album: Album, imgBmp: Bitmap)
    {
        val albumDisplay = AlbumDisplay()
        albumDisplay.albumID = album.id
        albumDisplay.photo = imgBmp
        albumDisplay.title = album.metadata!!.title
        albumViewModel.addAlbumDisplay(albumDisplay)
    }

    fun Fragment?.runOnUiThread(action: () -> Unit) {
        this ?: return
        if (!isAdded) return // Fragment not attached to an Activity
        activity?.runOnUiThread(action)
    }
}