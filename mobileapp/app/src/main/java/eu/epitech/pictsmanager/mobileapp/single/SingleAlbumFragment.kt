package eu.epitech.pictsmanager.mobileapp.single

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import eu.epitech.pictsmanager.mobileapp.ImageGridKotlinAdapter
import eu.epitech.pictsmanager.mobileapp.Metadata
import eu.epitech.pictsmanager.mobileapp.R
import kotlinx.android.synthetic.main.activity_main.*
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import eu.epitech.pictsmanager.mobileapp.album.Album
import eu.epitech.pictsmanager.mobileapp.album.AlbumFragment
import eu.epitech.pictsmanager.mobileapp.album.AlbumViewModel
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryViewModel
import eu.epitech.pictsmanager.mobileapp.gallery.Photo
import kotlinx.android.synthetic.main.single_album.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SingleAlbumFragment(private val album: Album) : Fragment() {
    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var listPhotos: MutableList<Photo>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.single_album, container, false)

        activity?.let { galleryViewModel = ViewModelProviders.of(it).get(GalleryViewModel::class.java) }

        this.activity?.bottomNavigationView?.visibility = View.VISIBLE
        listPhotos = mutableListOf()

        for(element in album.photoIdsList!!)
        {
            val service = RetrofitService.retrofitService()
            val imageRequest: Call<ResponseBody> = service.getPhotoData(element)
            val photo = Photo()
            photo.id = element

            imageRequest.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val imageResponse = response.body()
                    imageResponse?.let {
                        val bmp = BitmapFactory.decodeStream(it.byteStream())
                        photo.img = bmp
                        listPhotos.add(photo)
                    }
                }

                /**
                 * Invoked when a network exception occurred talking to the server or when an unexpected exception
                 * occurred creating the request or processing the response.
                 */
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.e("Photo", "Error : $t")
                }
            })
        }
        return view
    }

    private fun reload()
    {
        rvAlbumPhotos.adapter = ImageGridKotlinAdapter(context!!, listPhotos)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvAlbumPhotos.layoutManager = GridLayoutManager(context, 3)

        val mainHandler = Handler(Looper.getMainLooper())

        mainHandler.post(object : Runnable {
            override fun run() {
                reload()
                mainHandler.postDelayed(this, 2000)
            }
        })


        arrow_back_single.setOnClickListener {
            val transaction: FragmentTransaction =
                (context as FragmentActivity).supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, AlbumFragment())
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.addToBackStack(null)
            transaction.commit()
        }
        trash_single.setOnClickListener {
            deleteAlbum(album)
            activity?.let {
                val albumViewModel = ViewModelProviders.of(it).get(AlbumViewModel::class.java)
                albumViewModel.albums.value?.remove(album)
            }
            pushFragment(AlbumFragment(), context!!)
        }

        share_single.setOnClickListener {
            showDialogShare(album)
        }

        edit_single.setOnClickListener {
            showDialogEdit(album)
        }
    }

    private fun showDialogShare(album: Album) {
        val dialog = Dialog(context as Activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_share)
        val shareEditText = dialog.findViewById(R.id.et_share_user) as EditText
        val confirmBtn = dialog.findViewById(R.id.btn_share_confirm) as Button
        confirmBtn.setOnClickListener {
            if(shareEditText.text.toString().trim().isNotEmpty()){
                val arr = arrayOf(shareEditText.text.toString())
                sendUserAlbumAccess(album, arr)
                dialog.dismiss()
            }

        }
        val cancelBtn = dialog.findViewById(R.id.btn_share_cancel) as Button
        cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun showDialogEdit(album: Album) {
        val dialog = Dialog(context as Activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog)
        val titleEditText = dialog.findViewById(R.id.et_single_title) as EditText
        titleEditText.setText(album.metadata?.title)
        val descriptionEditText = dialog.findViewById(R.id.et_single_description) as EditText
        descriptionEditText.setText(album.metadata?.description)
        val tagsEditText = dialog.findViewById(R.id.et_single_tags) as EditText
        val join = album.metadata?.tags?.let { TextUtils.join(", ", it) }
        tagsEditText.setText(join)
        val cancelBtn = dialog.findViewById(R.id.btn_single_cancel) as Button
        cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        val confirmBtn = dialog.findViewById(R.id.btn_single_yes) as Button
        confirmBtn.setOnClickListener {
            val metadata = Metadata()
            metadata.title = titleEditText.text.toString()
            metadata.description = descriptionEditText.text.toString()
            val tags = tagsEditText.text.toString().split("\\s*,\\s*")
            metadata.tags = tags
            sendAlbumMetadata(album.id.toString(), metadata)
            album.metadata = metadata
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun sendAlbumMetadata(idAlbum: String, body: Metadata){
        val service = RetrofitService.retrofitService()
        val connectRequest: Call<ResponseBody> = service.putAlbumMetadata(idAlbum, body)
        connectRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body().let {

                }
            }
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }

    private fun deleteAlbum(album: Album){
        val service = RetrofitService.retrofitService()
        val connectRequest: Call<ResponseBody> = service.deleteAlbum(album.id.toString())
        connectRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body().let {
                    Log.d("TAN", "Deleted : $album.id")
                }
            }
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }

    private fun sendUserAlbumAccess(album: Album, body: Array<String>){
        val service = RetrofitService.retrofitService()
        val connectRequest: Call<ResponseBody> = service.addUserAccessToAlbum(album.id.toString(), body)
        connectRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body().let {
                    Log.d("TAN", "Add Access : $album.id")
                }
            }
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }

    private fun pushFragment(newFragment: Fragment, context: Context) {
        val transaction: FragmentTransaction =
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, newFragment)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}