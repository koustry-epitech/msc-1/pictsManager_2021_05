package eu.epitech.pictsmanager.mobileapp

class Metadata {
    var title: String? = null
        get() = field
        set(value){
            field = value
        }
    var description: String? = null
        get() = field
        set(value){
            field = value
        }

    var tags: List<String>? = null
        get() = field
        set(value){
            field = value
        }
}