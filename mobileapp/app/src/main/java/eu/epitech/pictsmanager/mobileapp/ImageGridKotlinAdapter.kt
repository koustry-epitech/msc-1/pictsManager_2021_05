package eu.epitech.pictsmanager.mobileapp

import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import androidx.core.view.drawToBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import eu.epitech.pictsmanager.mobileapp.gallery.Photo
import eu.epitech.pictsmanager.mobileapp.single.SinglePhotoFragment
import kotlinx.android.synthetic.main.item_grid_photo.view.*


class ImageGridKotlinAdapter(private val c: Context, private val images: List<Photo>) :
    RecyclerView.Adapter<ImageGridKotlinAdapter.ColorViewHolder>() {


    override fun getItemCount(): Int {
        return images.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        return ColorViewHolder(
            LayoutInflater.from(c).inflate(R.layout.item_grid_photo, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {

        holder.iv.setImageBitmap(images[position].img)

        holder.iv.setOnClickListener {
            //handle click event on image
            pushFragment(SinglePhotoFragment(images[position]), c)
        }
    }

    private fun pushFragment(newFragment: Fragment, context: Context) {
        val transaction: FragmentTransaction =
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, newFragment)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    class ColorViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val iv = view.ivPhoto as ImageView
    }


}