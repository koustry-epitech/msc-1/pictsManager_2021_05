package eu.epitech.pictsmanager.mobileapp.album

import android.graphics.Bitmap
import eu.epitech.pictsmanager.mobileapp.Metadata

class Album {
    var id: String? = null
    var metadata: Metadata? = null
    var photoIdsList: Array<String>? = null
}