package eu.epitech.pictsmanager.mobileapp

import android.app.Application
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        val token: String = AppPreferences.getToken()
        proceed(
            request()
                .newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .build()
        )
    }
}