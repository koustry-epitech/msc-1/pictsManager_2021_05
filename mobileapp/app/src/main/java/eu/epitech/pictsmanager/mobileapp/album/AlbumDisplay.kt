package eu.epitech.pictsmanager.mobileapp.album

import android.graphics.Bitmap

class AlbumDisplay {
    var albumID: String? = null
    var title: String? = null
    var photo: Bitmap? = null
}