package eu.epitech.pictsmanager.mobileapp.Search

import eu.epitech.pictsmanager.mobileapp.album.Album
import eu.epitech.pictsmanager.mobileapp.gallery.Photo

class Search {
    var albums: MutableList<Album>? = null
    var photos: MutableList<Photo>? = null
}