package eu.epitech.pictsmanager.mobileapp.gallery

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonArray
import eu.epitech.pictsmanager.mobileapp.R
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import eu.epitech.pictsmanager.mobileapp.Search.SearchViewModel
import eu.epitech.pictsmanager.mobileapp.databinding.GalleryFragmentBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GalleryFragment : Fragment() {

    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var searchViewModel: SearchViewModel

    private lateinit var binding: GalleryFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.gallery_fragment,
            container,
            false
        )
        activity?.let {
            galleryViewModel = ViewModelProviders.of(it).get(GalleryViewModel::class.java)
            galleryViewModel.photos.observe(this, Observer { updateList() })
            searchViewModel = ViewModelProviders.of(it).get(SearchViewModel::class.java)
            searchViewModel.photos.observe(this, Observer { updateSearch() })

            activity!!.getSharedPreferences("MY_APP", Context.MODE_PRIVATE)
        }


        binding.rvList.layoutManager = LinearLayoutManager(context)
        val searchBar = binding.searchBar.searchText
        searchBar.setText(searchViewModel.query.value)
        searchBar.addTextChangedListener(object : TextWatcher {
            /**
             * This method is called to notify you that, somewhere within
             * `s`, the text has been changed.
             */
            override fun afterTextChanged(s: Editable?) {
                searchViewModel.refreshSearch(searchBar.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        val service = RetrofitService.retrofitService()

        val photos = service.getPhotos()
        photos.enqueue(object: Callback<JsonArray> {
            override fun onResponse(call: Call<JsonArray>, response: Response<JsonArray>) {
                response.body()?.let {
                    runOnUiThread {
                        for(i in 0 until it.size())
                        {
                            val elem = it[i]
                            Log.i("elem", elem.asString)
                            val photo = Photo()
                            photo.id = elem.asString
                            if(galleryViewModel.isNewPhoto(photo.id!!))
                            {
                                galleryViewModel.downloadPhoto(photo)
                            }
                        }
                    }
                }
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<JsonArray>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })

        return binding.root
    }

    fun updateList() {
        Log.i("Update photo", "update")
        activity?.let { activity ->
            val galleryViewModel = ViewModelProviders.of(activity).get(GalleryViewModel::class.java)
            galleryViewModel.photos.value?.let{
                binding.rvList.adapter = GalleryAdapter(context!!, listOf<List<Photo>>(it))
            }
        }
    }


    private fun updateSearch() {
        if(!searchViewModel.query.value.isNullOrEmpty()) {
            searchViewModel.photos.value?.let {
                for (photo in it) {
                    val id = photo.id
                    photo.img = galleryViewModel.getExistingPhoto(id)!!.img
                }
            }
        }else{
            updateList()
        }
    }

    fun Fragment?.runOnUiThread(action: () -> Unit) {
        this ?: return
        if (!isAdded) return // Fragment not attached to an Activity
        activity?.runOnUiThread(action)
    }
}