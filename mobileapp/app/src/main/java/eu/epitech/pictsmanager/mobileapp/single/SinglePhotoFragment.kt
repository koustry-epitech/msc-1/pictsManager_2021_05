package eu.epitech.pictsmanager.mobileapp.single

import eu.epitech.pictsmanager.mobileapp.R
import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryFragment
import eu.epitech.pictsmanager.mobileapp.gallery.Photo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.single_image.*
import eu.epitech.pictsmanager.mobileapp.Metadata
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import eu.epitech.pictsmanager.mobileapp.gallery.GalleryViewModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SinglePhotoFragment(private val photo: Photo) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.single_image, container, false)
        this.activity?.bottomNavigationView?.visibility = View.VISIBLE
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        outputGalleryImage.setImageBitmap(photo.img)

        arrow_back_single.setOnClickListener {
            val transaction: FragmentTransaction =
                (context as FragmentActivity).supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, GalleryFragment())
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        trash_single.setOnClickListener {
            showDialogDelete(photo)
        }

        share_single.setOnClickListener {
            showDialogShare(photo)
        }

        edit_single.setOnClickListener {
            showDialogEdit(photo)
        }
    }

    private fun showDialogShare(photo: Photo) {
        val dialog = Dialog(context as Activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_share)
        val shareEditText = dialog.findViewById(R.id.et_share_user) as EditText
        val confirmBtn = dialog.findViewById(R.id.btn_share_confirm) as Button
        confirmBtn.setOnClickListener {
            if(shareEditText.text.toString().trim().isNotEmpty()){
                val arr = arrayOf(shareEditText.text.toString())
                sendUserAccess(photo.id.toString(), arr)
                dialog.dismiss()
            }

        }
        val cancelBtn = dialog.findViewById(R.id.btn_share_cancel) as Button
        cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun showDialogDelete(photo: Photo){
        val dialog = Dialog(context as Activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_delete)
        val confirmBtn = dialog.findViewById(R.id.btn_delete_confirm) as Button
        confirmBtn.setOnClickListener {
            deleteImage(photo)
            activity?.let {
                var galleryViewModel = ViewModelProviders.of(it).get(GalleryViewModel::class.java)
                galleryViewModel.photos.value?.remove(photo)
            }
            dialog.dismiss()
            activity?.supportFragmentManager?.popBackStack()
        }
        val cancelBtn = dialog.findViewById(R.id.btn_delete_cancel) as Button
        cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun showDialogEdit(photo: Photo) {
        val dialog = Dialog(context as Activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog)
        val titleEditText = dialog.findViewById(R.id.et_single_title) as EditText
        titleEditText.setText(photo.metadata?.title)
        val descriptionEditText = dialog.findViewById(R.id.et_single_description) as EditText
        descriptionEditText.setText(photo.metadata?.description)
        val tagsEditText = dialog.findViewById(R.id.et_single_tags) as EditText
        val join = photo.metadata?.tags?.let { TextUtils.join(", ", it) }
        tagsEditText.setText(join)
        val cancelBtn = dialog.findViewById(R.id.btn_single_cancel) as Button
        cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        val confirmBtn = dialog.findViewById(R.id.btn_single_yes) as Button
        confirmBtn.setOnClickListener {
            val metadata = Metadata()
            metadata.title = titleEditText.text.toString()
            metadata.description = descriptionEditText.text.toString()
            val tags = tagsEditText.text.toString().split("\\s*,\\s*")
            metadata.tags = tags
            sendImageMetadata(photo.id.toString(), metadata)
            photo.metadata = metadata
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun sendImageMetadata(idImage: String, body: Metadata){
        val service = RetrofitService.retrofitService()
        val connectRequest: Call<ResponseBody> = service.postPhotoMetadata(idImage, body)
        connectRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body().let {

                }
            }
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }

    private fun deleteImage(photo: Photo){
        val service = RetrofitService.retrofitService()
        val connectRequest: Call<ResponseBody> = service.deletePhoto(photo.id.toString())
        connectRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body().let {
                    Log.d("TAN", "Deleted : $photo.id")
                }
            }
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }


    private fun sendUserAccess(idImage: String, body: Array<String>){
        val service = RetrofitService.retrofitService()
        val connectRequest: Call<ResponseBody> = service.addUserAccessToPhoto(idImage, body)
        connectRequest.enqueue(object: Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body().let {

                }
            }
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("TAN", "Error : $t")
            }
        })
    }
}