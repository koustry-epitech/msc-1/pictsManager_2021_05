package eu.epitech.pictsmanager.mobileapp.gallery

import android.graphics.Bitmap
import android.media.Image
import com.google.gson.annotations.SerializedName
import eu.epitech.pictsmanager.mobileapp.Metadata
import java.io.File
import java.time.LocalDate
import java.util.*

class Photo {
    var id: String? = null
        get() = field
        set(value){
            field = value
        }
    
    var img: Bitmap? = null
        get() = field
        set(value){
            field = value
        }

    var metadata: Metadata? = null
        get() = field
        set(value){
            field = value
        }
}