package eu.epitech.pictsmanager.mobileapp.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import eu.epitech.pictsmanager.mobileapp.R
import eu.epitech.pictsmanager.mobileapp.camera.CameraFragment
import eu.epitech.pictsmanager.mobileapp.databinding.LoginFragmentBinding
import kotlinx.android.synthetic.main.activity_main.*

class LoginFragment: Fragment(){
    private lateinit var binding: LoginFragmentBinding

    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)

        activity?.let {
            userViewModel = ViewModelProviders.of(it).get(UserViewModel::class.java)
        }

        binding.buttonToRegister.setOnClickListener{
            this.changeFragment(RegisterFragment())
        }

        binding.buttonLogin.setOnClickListener{
            userViewModel.connect(binding.tfUsernameLogin.text.toString(), binding.tfPasswordLogin.text.toString(), this)
        }

        this.activity?.bottomNavigationView ?.visibility = View.GONE
        return binding.root
    }

    //function called after the success of the connect request to change of fragment
    fun connectAfterRequest() {
        this.changeFragment(CameraFragment())

    }

    private fun changeFragment(fragment : Fragment) {
        this.activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.container, fragment)
            ?.commit()
    }
}