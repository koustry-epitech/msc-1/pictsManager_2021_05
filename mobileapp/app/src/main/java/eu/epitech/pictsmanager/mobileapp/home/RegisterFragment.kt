package eu.epitech.pictsmanager.mobileapp.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import eu.epitech.pictsmanager.mobileapp.R
import eu.epitech.pictsmanager.mobileapp.databinding.RegisterFragmentBinding
import kotlinx.android.synthetic.main.activity_main.*

class RegisterFragment: Fragment(){

    private lateinit var binding: RegisterFragmentBinding

    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.register_fragment, container, false)
        activity?.let {
            userViewModel = ViewModelProviders.of(it).get(UserViewModel::class.java)
        }

        binding.buttonToLogin.setOnClickListener {
            this.changeFragment(LoginFragment())
        }

        binding.buttonRegister.setOnClickListener{
            userViewModel.register(binding.tfUsernameRegister.text.toString(), binding.tfPasswordRegister.text.toString(), this)
        }
        this.activity?.bottomNavigationView ?.visibility = View.GONE
        return binding.root
    }


    //function called after the success of the register request to change of fragment
    fun registerAccomplished() {
        this.changeFragment(LoginFragment())
    }

    private fun changeFragment(fragment : Fragment) {
        this.activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.container, fragment)
            ?.commit()
    }
}