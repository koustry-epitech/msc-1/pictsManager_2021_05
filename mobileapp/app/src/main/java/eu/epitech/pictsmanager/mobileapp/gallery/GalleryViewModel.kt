package eu.epitech.pictsmanager.mobileapp.gallery

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import eu.epitech.pictsmanager.mobileapp.Metadata
import eu.epitech.pictsmanager.mobileapp.RetrofitService
import eu.epitech.pictsmanager.mobileapp.album.Album
import eu.epitech.pictsmanager.mobileapp.album.AlbumFragment
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class GalleryViewModel : ViewModel() {
    private val _photos = MutableLiveData<MutableList<Photo>>()
    val photos: LiveData<MutableList<Photo>> get() = _photos

    /*fun searchByDate(date: String) {
        var parsedDate = LocalDate.parse(date)

            DateTimeFormatter.ofPattern("EEEE, MMMM dd, yyyy")
            DateTimeFormatter.ofPattern("MMMM dd, yyyy")
            DateTimeFormatter.ofPattern("dd/MM/yyyy")
            DateTimeFormatter.ofPattern("dd MMMM,yyyy")
            DateTimeFormatter.ofPattern("dd'th' MMMM,yyyy")
            DateTimeFormatter.ofPattern("dd MMM, yyyy")

    }*/

    // Download the image of a specified photo
    fun downloadPhoto(photo: Photo) {
        var photoReturn = photo
        if(_photos.value.isNullOrEmpty()) {
            imageRequest(photoReturn)
        }
        else {
            val index = _photos.value!!.binarySearch { String.CASE_INSENSITIVE_ORDER.compare(it.id, photoReturn.id) }
            if(index >= 0) {
                photoReturn = _photos.value!![index]
                if(_photos.value!![index].img !== null){
                    retrieveMetadata(photoReturn)
                }
                else {
                    imageRequest(photoReturn)
                }
            }
            else {
                imageRequest(photoReturn)
            }
        }
    }


    fun retrieveMetadata(photo: Photo) {
        if(photo.metadata == null)
        {
            val service = RetrofitService.retrofitService()
            val metadataRequest: Call<Metadata> = service.getPhotoMetaData(photo.id!!)
            metadataRequest.enqueue(object: Callback<Metadata> {
                override fun onResponse(call: Call<Metadata>, response: Response<Metadata>) {
                    val metadataResponse = response.body()
                    metadataResponse?.let { photo.metadata = metadataResponse }
                    Log.e("Metadata", photo.id!!.last().toString())
                    addPhoto(photo)
                }

                /**
                 * Invoked when a network exception occurred talking to the server or when an unexpected exception
                 * occurred creating the request or processing the response.
                 */
                override fun onFailure(call: Call<Metadata>, t: Throwable) {
                    Log.e("Metadata", "Error : $t")
                    addPhoto(photo)
                }
            })
        }
        addPhoto(photo)
    }

    private fun imageRequest(photo: Photo) {
        val service = RetrofitService.retrofitService()
        val imageRequest: Call<ResponseBody> = service.getPhotoData(photo.id!!)

        imageRequest.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                val imageResponse = response.body()
                imageResponse?.let {
                    val bmp = BitmapFactory.decodeStream(it.byteStream())
                    photo.img = bmp
                }
                Log.e("Photo", photo.id!!.last().toString())
                retrieveMetadata(photo)
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("Photo", "Error : $t")
            }
        })
    }

    fun isNewPhoto(id: String) : Boolean {
        return if( _photos.value.isNullOrEmpty())
            true
        else {
            val index = _photos.value!!.binarySearch { String.CASE_INSENSITIVE_ORDER.compare(it.id, id) }
            return if(index >= 0) {
                if(_photos.value!![index].img === null || _photos.value!![index].metadata === null){
                    true
                } else {
                    true
                }
            } else
                false
        }
    }

    fun addPhoto(photo: Photo) {
        if(photos.value.isNullOrEmpty())
        {
            val list : MutableList<Photo> = mutableListOf()
            list.add(photo)
            _photos.value = list

        }
        else {
            val index = _photos.value!!.binarySearch { String.CASE_INSENSITIVE_ORDER.compare(it.id, photo.id) }
            if(index == -1)
                photos.value!!.add(photo)
            else
            {
                val photoSearch = photos.value?.find{ it.id == photo.id}
                _photos.value?.remove(photoSearch)
                photos.value!!.add(photo)
            }

        }
    }

    fun getPhoto(album: Album, photoId: String, albumFragment: AlbumFragment) {
        val service = RetrofitService.retrofitService()
        val imageRequest: Call<ResponseBody> = service.getPhotoData(photoId)

        imageRequest.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                val imageResponse = response.body()
                imageResponse?.let {
                    val bmp = BitmapFactory.decodeStream(it.byteStream())
                    albumFragment.addAlbumImage(album, bmp)
                }
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("Photo", "Error : $t")
            }
        })
    }

    fun getExistingPhoto(id: String?): Photo? {
        if(!_photos.value.isNullOrEmpty())
        {
            val index = _photos.value!!.binarySearch { String.CASE_INSENSITIVE_ORDER.compare(it.id, id) }
            return _photos.value!![index]
        }
        return null
    }
}